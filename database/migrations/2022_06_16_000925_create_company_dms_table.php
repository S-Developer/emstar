<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyDmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_dms', function (Blueprint $table) {
            $table->id();
            $table->string("cmp_id");
            $table->string("title");
            $table->string("description");
            $table->string("search_key");
            $table->string("user_id")->nullable();
            $table->string("category_id");
            $table->string("url");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_dms');
    }
}
