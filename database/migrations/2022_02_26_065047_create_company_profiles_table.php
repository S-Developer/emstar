<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profiles', function (Blueprint $table) {
            $table->id();
            $table->string("cmp_id");
            $table->string("name");
            $table->string("address");
            $table->string("reg_number");
            $table->string("date_registered");
            $table->string("bank_acc_number");
            $table->string("bank");
            $table->string("branch_code");
            $table->string("country");
            $table->string("logo_url");
            $table->string("web_address");
            $table->string("contact_email");
            $table->string("contact_phone");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profiles');
    }
}
