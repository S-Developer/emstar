<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVistorsRecordsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vistors_records', function (Blueprint $table) {
            $table->id();
            $table->string("national_id");
            $table->string("full_name");
            $table->string("phone_number");
            $table->string("vehicle_registration")->nullable();
            $table->date("date");
            $table->time("time_in");
            $table->time("time_out")->nullable();
            $table->string("office")->nullable();
            $table->string("purpose");
            $table->string("tmp")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vistors_records');
    }
}
