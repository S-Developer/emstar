<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->string("cmp_id");
            $table->string("emp_id");
            $table->string("title");
            $table->string("fullname");
            $table->string("phonenumber");
            $table->string("email");
            $table->dateTime("start_date");
            $table->dateTime("end_date");
            $table->string("url")->nullable();
            $table->string("virtual_platform_id")->nullable();
            $table->boolean('bln_is_accepted')->default(0)->change();
            $table->boolean('bln_is_virtual')->default(0)->change();
            $table->boolean('bln_active')->default(1)->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
