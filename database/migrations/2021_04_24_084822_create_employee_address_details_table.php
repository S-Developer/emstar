<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAddressDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_address_details', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id')->nullable();
            $table->string('town')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('province')->nullable();
            $table->string('county')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_address_details');
    }
}
