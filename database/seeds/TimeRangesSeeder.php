<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TimeRangesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\TimeRange::create([
            'name' => '15 munites',
            'value' => '15'
        ]);
        App\Models\TimeRange::create([
            'name' => '30 munites',
            'value' => '30'
        ]);
        App\Models\TimeRange::create([
            'name' => '45 munites',
            'value' => '45'
        ]);
        App\Models\TimeRange::create([
            'name' => 'Hour',
            'value' => '60'
        ]);

        App\Models\TimeRange::create([
            'name' => '1 Hour 30 munites',
            'value' => '90'
        ]);

        App\Models\TimeRange::create([
            'name' => '2 Hours',
            'value' => '120'
        ]);
        //
    }
}
