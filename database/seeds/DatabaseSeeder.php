<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\Models\TimeRange::create([
            'name' => '15 munites',
            'value' => '15'
        ]);
        App\Models\TimeRange::create([
            'name' => '30 munites',
            'value' => '30'
        ]);
        App\Models\TimeRange::create([
            'name' => '45 munites',
            'value' => '45'
        ]);
        App\Models\TimeRange::create([
            'name' => 'Hour',
            'value' => '60'
        ]);

        App\Models\TimeRange::create([
            'name' => '1 Hour 30 munites',
            'value' => '90'
        ]);

        App\Models\TimeRange::create([
            'name' => '2 Hours',
            'value' => '120'
        ]);
        // $this->call(UsersTableSeeder::class);
        // DB::table('company_profiles')->insert([
        //     'name' => Str::random(10),
        //     'email' => Str::random(10).'@gmail.com',
        //     'password' => Hash::make('password'),
        // ]);
    }
}
