// created on 19 june 2021
function showNotification(message, status) {
    messagetype = "bg-success";
    if (status == "error") {
        messagetype = "bg-danger";

    }
    $(document).Toasts('create', {
        class: messagetype,
        title: message,
    });
}

function getLeaveApplications() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/employees_leave_pending",
        success: function(response) {

            $('#leave_application_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "emp_id"
                    },
                    {
                        "data": "leave_type"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "start_date"
                    },
                    {
                        "data": "end_date"
                    },

                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return ' <a  target="_blank" href="http://localhost:8000/Documents/' + row.supporting_document_url + '" class="btn btn-info btn-sm"><i class="fa fa-link"></i></a> &nbsp<Button onclick="approve(' + row.id + ')" class="btn btn-success btn-sm"> &nbsp<i class="fa fa-check"></i></Button>&nbsp<Button onclick="reject(' + row.id + ')" class="btn btn-danger btn-sm"> &nbsp<i class="fa fa-times"></i></Button>'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}

function getDeductions() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/deductions",
        success: function(response) {

            $('#leave_application_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "id"
                    },
                    {
                        "data": "item"
                    },
                    {
                        "data": "value"
                    },
                    {
                        "data": "grade_id"
                    },


                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return '<Button onclick="disable_deduction(' + row.id + ')" class="btn btn-danger btn-sm"> &nbsp<i class="fa fa-trash"></i></Button>'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}

function getEarnings() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/earnings",
        success: function(response) {

            $('#leave_application_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "id"
                    },
                    {
                        "data": "item"
                    },
                    {
                        "data": "value"
                    },
                    {
                        "data": "grade_id"
                    },

                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return '<Button onclick="disable_deduction(' + row.id + ')" class="btn btn-danger btn-sm"> &nbsp<i class="fa fa-trash"></i></Button>'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}

function getWorkLeave() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/employees_leave",
        success: function(response) {

            $('#leave_application_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "emp_id"
                    },
                    {
                        "data": "leave_type"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "start_date"
                    },
                    {
                        "data": "end_date"
                    },
                    {
                        "data": "status"
                    },


                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return ' <a  target="_blank" href="http://localhost:8000/Documents/' + row.supporting_document_url + '" class="btn btn-danger btn-sm"><i class="fa fa-link"></i></a> &nbsp'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}

function getGrades() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/grades",
        success: function(response) {

            $('#leave_application_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "id"
                    },
                    {
                        "data": "value"
                    },
                    {
                        "data": "description"
                    },

                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return ' <a  href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}

function getTaxRates() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/rates",
        success: function(response) {

            $('#leave_application_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "id"
                    },
                    {
                        "data": "item"
                    },
                    {
                        "data": "value"
                    },

                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return ' <a  href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}
// approve leave application
function approve(leave_id) {
    $.ajax({
        type: "post",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/approve_leave/" + leave_id,

        success: function(response) {
            showNotification("Leave application is approved", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
}
// reject leave application
function reject(leave_id) {
    $.ajax({
        type: "post",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/reject_leave/" + leave_id,

        success: function(response) {
            showNotification("Leave application is rejected", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
}

$('#leave_form_document').submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: 'POST',
        url: "{{ url('save_leave_documents')}}",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
            this.reset();
            alert('Image has been uploaded successfully');
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$("#save_leave_app").on('click', function(e) {
    var form = $(".leave_form");
    alert(form.serialize())
    $.ajax({
        type: "post",
        url: "/save_employee_leave",
        data: form.serialize(),
        success: function(store) {
            showNotification("Leave application applied", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

$("#save_grade").on('click', function(e) {
    var form = $(".grade_form");
    $.ajax({
        type: "post",
        url: "/save_new_grade",
        data: form.serialize(),
        success: function(store) {
            showNotification("Grade Saved", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});


$("#save_deductions").on('click', function(e) {
    var form = $(".deductions_form");
    $.ajax({
        type: "post",
        url: "/save_employee_deductions",
        data: form.serialize(),
        success: function(store) {
            showNotification("Added", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

$("#save_earnings").on('click', function(e) {
    var form = $(".earnings_form");
    $.ajax({
        type: "post",
        url: "/save_employee_earnings",
        data: form.serialize(),
        success: function(store) {
            showNotification("Added", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

$("#save_tax").on('click', function(e) {
    var form = $(".tax_form");
    $.ajax({
        type: "post",
        url: "/save_new_rates",
        data: form.serialize(),
        success: function(store) {
            showNotification("Added", "success");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

function getPublicHolidays() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/holidays",
        success: function(response) {

            $('#public_holiday_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "holiday_date"
                    },
                    {
                        "data": "localName"
                    },


                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}

$("#save_comp_info").on('click', function(e) {
    var form = $(".company_profile_form");
    $.ajax({
        type: "post",
        url: "/save_comp_profile",
        data: form.serialize(),
        success: function(store) {
            showNotification("Profile Saved");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});


function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}



function getVisitors() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/vistors",
        success: function(response) {

            $('#vistorstable').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],

                "data": response,
                "columns": [{
                        "data": "date"
                    },
                    {
                        "data": "full_name"
                    },
                    {
                        "data": "phone_number"
                    },
                    {
                        "data": "vehicle_registration"
                    },
                    {
                        "data": "time_in"
                    },
                    {
                        "data": "time_out"
                    },

                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return '<button data-val="' + row.id + '" data-toggle="modal" data-target="#my-modal" class="btn btn-sm btn-primary"><i class="fa fa-clock"></i></button>'
                        }
                    }

                ]
            }).buttons().container().appendTo($('#example'));
        }

    });
}




$('#my-modal').on('show.bs.modal', function(event) {
    var myVal = $(event.relatedTarget).data('val');
    document.getElementById("row_id").setAttribute('value', myVal);
});

$("#save_visitor_record").on('click', function(e) {
    var form = $(".record");
    $.ajax({
        type: "post",
        url: "/save_visitor_record",
        data: form.serialize(),
        success: function(store) {
            showNotification("Record updated");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

$("#save_visitor_time_out").on('click', function(e) {
    var form = $(".record_time_out");
    $.ajax({
        type: "post",
        url: "/save_visitor_time_out",
        data: form.serialize(),
        success: function(store) {
            showNotification("Record updated");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

function getEvents() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/events",
        success: function(response) {

            $('#public_holiday_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "date"
                    },

                    {
                        "data": "start_time"
                    },

                    {
                        "data": "end_time"
                    },


                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}
$("#save_event").on('click', function(e) {
    var form = $(".record_time_out");
    $.ajax({
        type: "post",
        url: "/save_event",
        data: form.serialize(),
        success: function(store) {
            showNotification("Record updated");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});

function getNotices() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/api/notices",
        success: function(response) {

            $('#notices_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [{
                        "data": "created_at"
                    },
                    {
                        "data": "notice"
                    },



                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}





function getDms() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: "http://127.0.0.1:8000/dms_files",
        success: function(response) {
            console.log(response);

            $('#dms_table').DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "data": response,
                "columns": [
                    {
                        "data": "name"
                    },
                    {
                    "data":"title"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "search_key"
                    },
                    {
                        "data": "id",
                        "render": function(data, type, row) {
                            return ' <a href="/' + row.url + '" target="blank" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-view"></i> view</a>'
                        }
                    }
                ]
            }).buttons().container().appendTo($('#example'));
        }
    });
}





$("#send_notice").on('click', function(e) {
    var form = $(".send_u_notice");
    $.ajax({
        type: "post",
        url: "/send_notice",
        data: form.serialize(),
        success: function(store) {
            showNotification("Record updated");
            window.setTimeout(function() {
                window.location.reload();
            }, 3000);
        },
        error: function(e) {
            alert(e.Message)
        }
    });
});