<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeePositionController;
use App\Http\Controllers\DmsCategoryController;
use App\Http\Controllers\CompanyDmsController;
use App\Http\Controllers\AppointmentController;
use App\Models\TimeRange;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
Route::get('/all_employees','EmployeeController@index');
Route::get('/employees', function () {
    return view('EmployeeCrud/employees');
});


Route::get('/employees_leave_applications',function(){
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('EmployeeLeave/leave_application');
});

Route::get('/employees_leave_calender',function(){
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('EmployeeLeave/leave_calender');
});

Route::get('/employees_leave',function(){
    return view('EmployeeLeave/work_leaves');
});

Route::get('/currency',function(){
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('SystemSettings/currency');
});


Route::get('new_employee', function(){
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('EmployeeCrud/new_employee');
});

Route::get('/qualifications_details', function () {
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('EmployeeCrud/qualifications_details');
});

Route::get('/documents_details', function () {
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('EmployeeCrud/documents_details');
});

Route::get('/notes_details', function () {
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('EmployeeCrud/notes_details');
});


// controllers
Route::get('employees_details/{emp_id},{FirstName},{LastName}','EmployeeController@emp_details'); 
Route::get('employee_profile/{emp_id},{FirstName},{LastName}','EmployeeController@emp_profile');
// Route::get('/employees','EmployeeController@index');
Route::post('/new_employee','EmployeeController@store');
Route::post('/update_personal_details','EmployeeController@update');
Route::post('/update_next_of_kin_details','NextOfKinController@update');
Route::post('/personal_details','EmployeeController@show');
Route::post('/address_details','EmployeeAddressDetailsController@show');
Route::post('/update_address_details','EmployeeAddressDetailsController@update');
Route::post('/bank_details','BankDetailsController@show');
Route::post('/update_bank_details','BankDetailsController@update');
Route::post('/employee_qualifications','QualificationsController@show');
Route::post('/add_employee_qualifications','QualificationsController@store');

Route::get('/all_qualification','QualificationsTypesController@show');
Route::post('/save_new_qualifications_types','QualificationsTypesController@store');

Route::post('/documents_details','EmployeeDocumentsController@show');
Route::post('/save_documents',"EmployeeDocumentsController@store");

Route::post('/add_employee_experience','ExperienceController@store');

Route::post('notes_details','EmployeeNotesController@show');
Route::post('add_employee_notes','EmployeeNotesController@store');

Route::get('/employee_position',[EmployeePositionController::class,'show']);
Route::post('add_system_position','PositionsController@store');

Route::post('add_employee_position','EmployeePositionController@store');

//employee leave days

// Route::get('employees_leave','WorkLeaveController@show');
Route::post('/save_employee_leave','WorkLeaveController@store'); 
Route::post('/approve_leave','WorkLeaveController@updateStatus');
Route::get('/public_holidays', function () {return view('Calender/public_holidays');});

//events
Route::get('/company_events', function () {return view('Calender/events');});
Route::post('/save_event','CompanyEventController@store');




Route::get('leave_types','LeaveTypesController@show'); 
Route::get('apply_leave','WorkLeaveController@applyLeave');
Route::post('/save_leave_types','LeaveTypesController@store');

Route::post('/save_leave_documents','WorkLeaveController@update');

//register
Route::get('employee_attendence','EmployeeAttendanceController@show');

//payroll
Route::get('/Earnings', function () {return view('Payroll/salary_earnings');});
Route::get('/Deductions', function () {return view('Payroll/salary_deductions');});
Route::get('/grades', function () {return view('Payroll/salary_grades');});
Route::get('/new_deductions','SalaryGradesController@index');
Route::get('/new_earnings','SalaryEarningsController@index');

// Route::get('/', function () {return view('Payroll/new_earnings');});

Route::get('/new_grade', function () {return view('Payroll/new_grade');});
Route::post('/save_new_grade','SalaryGradesController@store');
Route::post('/save_employee_deductions','SalaryDeductionController@store');
Route::post('/save_employee_earnings','SalaryEarningsController@store');

Route::get('/tax', function () { return view('Payroll/tax_table');});
Route::get('/new_tax', function () {return view('Payroll/new_tax_rates');});

Route::post('/save_new_rates','TaxRateController@store');
// Salarie contract
Route::get('/employee_new_contract/{emp_id}','EmployeeContractController@index');
Route::get('/employee_payslip/{emp_id}','EmployeeContractController@payslip');


//company profile
Route::get('/company_profile','CompanyProfileController@show');
Route::get('/company_branding','CompanyProfileController@companyBranding');
Route::post('/save_comp_profile','CompanyProfileController@store');
Route::post('/update_logo','CompanyProfileController@updateLogo');


//Dms 
Route::post('/add_doc_category',[DmsCategoryController::class,'create']);


//visitors and attendency 


Route::get('/visitors', function () {
    if(!Auth::check()){
        return Redirect::to("/")->withSuccess('Opps! You do not have access');
     }
    return view('Register/visitor_register');
});

Route::post('/save_visitor_record','VistorsRecordController@create');
Route::post('/save_visitor_time_out','VistorsRecordController@timeOut');


// notices
Route::get('/notices', function () {return view('Notices/notices');});
Route::post('/send_notice','CompanyNoticeController@store');

//dms
Route::get('/dms_categories',[DmsCategoryController::class,'show']);
Route::get('/new_dms' ,[CompanyDmsController::class,'add_new']);
Route::post('/save_dms',[CompanyDmsController::class,'store']);
Route::get('/dms_files',[CompanyDmsController::class,'index']);
Route::get('/delete_dms_cate/{id}',[DmsCategoryController::class,'destroy']);
Route::get('dms/', function () {
    return view('Dms/company_dms');
    
});


//appointments 
Route::get('/new_appointments', function () {
    $time_range = TimeRange::all();
    return view('Appointments/new_appointment',compact("time_range"));
});

Route::get('/all_appointments', function () {
    return view('Appointments/all_appointments');
});

Route::post('/save_appointment',[AppointmentController::class,'store']);
Route::get('/appointments',[AppointmentController::class,'index']);
});

