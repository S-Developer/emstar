<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
 Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function(){
    Route::get('/user', function( Request $request ){
      return $request->user();
    });
  });

Route::post('/register', 'Auth\UserAuthController@register');
Route::post('/login', 'Auth\UserAuthController@login');

// all employees
Route::get('/employees','EmployeeController@index');

// leave applications
Route::get('employees_leave','WorkLeaveController@show');
Route::get('calendar','WorkLeaveController@showCalendar');
Route::get('employees_leave_pending','WorkLeaveController@pendingApproval');
Route::post('approve_leave/{id}','WorkLeaveController@approve');
Route::post('reject_leave/{id}','WorkLeaveController@reject');

Route::get('deductions','SalaryDeductionController@show');
Route::get('earnings','SalaryEarningsController@show');
Route::get('grades','SalaryGradesController@show');
Route::get('rates','TaxRateController@show');

//public holidays
Route::get('create_public_holiday','PublicHolidayController@create');
Route::get('holidays','PublicHolidayController@show');

//Register
Route::get('vistors','VistorsRecordController@show');

//Events
Route::get('events','CompanyEventController@show');

//Noticies
Route::get('notices','CompanyNoticeController@show');

//DMS
Route::get('dms/{id}','DmsCategoryController@index');


