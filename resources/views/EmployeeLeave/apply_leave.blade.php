@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Leave Database</h5>
    <span>Apply For Leave</span>
  </div>
  <div class="col-md-2">
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form class="leave_form">
                <div class="form-group">
                <select id="emp_id" name="emp_id" class="form-control">
                  @foreach ($employee as $item)
                  <option value="{{$item->emp_id}}">{{$item->FirstName}}{{$item->LastName}}</option>
                  @endforeach
                </select>
                </div>

                <div class="form-group">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <select id="leave_type" name="leave_type" class="form-control">
                    @foreach ($leaveTypes as $itm)
                    <option value="{{$itm->type}}">{{$itm->type}}</option>
                    @endforeach
                  </select>
                  </div>

                 <div class="form-group">
                  <p for="inputEmail3">Description</p>
                    <input type="text" class="form-control" maxlength="50" id="description" name="description" required>
                 </div>

                 <div class="form-group">
                  <p for="inputEmail3">Start Date</p>
                    <input type="date" class="form-control" id="start_date" name="start_date" required>
                 </div>

                 <div class="form-group">
                  <p for="inputEmail3">End Date</p>
                    <input type="date" class="form-control" id="end_date" name="end_date" required>
                 </div>
                    <div class="form-group">
                        <p for="inputEmail3">Document Supporting if available</p>
                      <input type="file" name="image" placeholder="Choose image" id="image">
                    </div>
                    <div class="form-group">
                        <button type="button" id="save_leave_app" class="btn btn-primary">Save</button>
                     </div>

              </div>


            </div>
        </div>

  </div>
    </div>
</section>
@endsection