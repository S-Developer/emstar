@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Leave Database</h5>
    <span>All Approved</span>
  </div>
  <div class="col-md-2">
    <a  href="{{'/employees_leave_calender'}}"><Button class="btn btn-primary btn-sm"><i class="nav-icon fas fa-calendar-alt"></i> Leave Calender</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div id="example2"></div>
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="leave_application_table" class="table table-sm table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>Emp_id#</th>
                <th>Leave Type</th>
                <th>Description</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th></th>
              </tr>
          </thead>

        </table>

      </div>
    </div>
      </div>
    </div>

</section>


<script>
  $(document).on("click", ".user_dialog", function () {
     var id = $(this).data('id');
     $(".modal-body #application_id").val(id);
     $("#UserDialog").modal();

});

$(document).ready(function() {
  getWorkLeave();
});


  </script>
@endSection