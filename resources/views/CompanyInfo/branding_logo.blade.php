@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Company Branding</h5>
    <span>Profile</span>
  </div>
  <div class="col-md-2">

  </div>
</section>
<section class="content">
    <div class="card">
        <form class ="company_profile_form" method="post" action="/update_logo" enctype="multipart/form-data">
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <img src="{{$company_profile->logo_url}}" class="img-thumbnail">
            </div>
</div>
      <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
                <div class="col-sm-10">
               
                <input type="hidden" name="cmp_id" id="cmp_id" value="{{$company_profile->cmp_id}}">
                <input type="hidden" class="form-control" id="name" name="name" placeholder="Company Name" value="{{$company_profile->name}}">
               </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id="address" name="address" placeholder="Address" value="{{$company_profile->address}}">
                </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id="bank" name="bank" placeholder="Bank" value="{{$company_profile->bank}}">
                </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id="branch_code" name="branch_code" placeholder="Branch Code" value="{{$company_profile->branch_code}}" >
                </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-10">
                <input type="hidden" class="form-control" id="contact_phone" name="contact_phone" placeholder="Contact Number" value="{{$company_profile->contact_phone}}">
                </div>
          </div>
          
               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">logo Url</p>
                <div class="col-sm-10">
                <input type="hidden" class="form-control" id="logo_url" name="logo_url" value="{{$company_profile->logo_url}}">
                <input type="file" name="image" id="image"/>
               
                </div>
               </div>

          </div>
          <div class="col-md-6">

            <div class="form-group row">
                <div class="col-sm-10">
                <input type="hidden" class="form-control" id="reg_number" name="reg_number" placeholder="reg_number" value="{{$company_profile->reg_number}}">
                </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id="date_registered" name="date_registered" placeholder="Date of Registered" value="{{$company_profile->date_registered}}">
                </div>
               </div>
               <div class="form-group row">
                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id="bank_acc_number" name="bank_acc_number" placeholder="Account Number" value="{{$company_profile->bank_acc_number}}">
                </div>
               </div>

               <div class="form-group row">
                <div class="col-sm-10">
                <input type="hidden" class="form-control" id="country" name="country" placeholder="Account Number" value="{{$company_profile->country}}">
              
                </div>
               </div>
               <div class="form-group row">
                <div class="col-sm-10">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" class="form-control" id="contact_email" name="contact_email" placeholder="Email Address" value="{{$company_profile->contact_email}}">
                </div>
               </div>
          </div>
          <div class="form-group row">
                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id="web_address" name="web_address" placeholder="Web Address" value="{{$company_profile->web_address}}">
                </div>
               </div>


      </div>
      <div class="form-group row">
        <div class="col-sm-10">
         <Button type="submit" class="btn btn-sm btn-primary">Save logo</Button>
        </div>
    </div>
    </div>
    </form>

      </div>
    </div>
</form>
</section>
@endsection