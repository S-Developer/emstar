@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>System Settings</h5>
    <span>All Qualifications</span>
  </div>
  <div class="col-md-2">
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add New</button>
  </div>
</section>
<section class="content">
    <div class="card">
    <div class="card-body">
<div class="table-responsive">
        <table class="table table-sm">
            <thead>
                <tr>
                  <th>Qualification</th>
                  <th>Description</th>
                  <th>Created</th>
                  <th>Updated</th>
                </tr>
            </thead>
            <tbody>
                @foreach($qualifications as $qualification)
                <tr>
                <td>{{$qualification->qualification_type}}</td>
                <td>{{$qualification->description}}</td>
                <td>{{$qualification->created_at}}</td>
                <td>{{$qualification->updated_at}}</td>
                <td>
                  <a href="/employees_details/{{$qualification['emp_id']}}"><button class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button></a>
                </td>
                </td>
                @endforeach
            </tbody>
        </table>
</div>

    </div>
      </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Add Qualification</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
            <form class="newQualifications" method="post" action="/new_qualifications">
              <div class="form-group">
                <p for="inputEmail3">Qualification</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Qualification">
               </div>
               <div class="form-group">
                <p for="inputEmail3">Description</p>
                  <input type="text" class="form-control" id="description" name="description" placeholder="Description">
               </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="save_qualifications" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>
</section>
    <script>
      $("#save_qualifications").on('click',function(e) {
  var form= $(".newQualifications");
    $.ajax({
        type: "post",
        url: "/save_new_qualifications_types",
        data: form.serialize(),
        success: function(store) {
          $("#myModal").modal("hide")
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});
      </script>
@endsection