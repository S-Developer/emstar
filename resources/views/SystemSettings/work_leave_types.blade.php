@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Leave</h5>
    <span>All Leave Types</span>
  </div>
  <div class="col-md-2">
    <Button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#new_leave_type">New Leave Type</Button>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-6">
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title">Leave Types</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body p-0">
        <table class="table">
          <thead>
              <tr class="table">
                <th>id#</th>
                <th>Leave Type</th>
                <th></th>
              </tr>
          </thead>
          <tbody>
              @foreach($leaveTypes as $wlt)
              <tr>
              <td>{{$wlt->id}}</td>
              <td>{{$wlt->type}}</td>
              </td>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
      </div>
    </div>



    <div id="new_leave_type" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">New Leave Type</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
            <form class="leave_form_type" method="post" action="/new_qualifications">
              <div class="form-group">
                <p for="inputEmail3">Leave Type</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" id="type" name="type" required>
               </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="save_type" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>


    <script>
      $("#save_type").on('click',function(e) {
  var form= $(".leave_form_type");
    $.ajax({
        type: "post",
        url: "/save_leave_types",
        data: form.serialize(),
        success: function(store) {
          $("#new_leave_type").modal("hide")
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});
      </script>


</section>
@endSection