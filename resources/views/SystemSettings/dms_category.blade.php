@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Documents Category</h5>
    <span>Categories</span>
  </div>
  <div class="col-md-2">
    <div class="btn-group" role="group">
    <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#newPosition">New Category</button>
    </div>
  </div>
  </div>
</section>
<section class="content">
  <div class="col-md-6">
  <div class="card">
    <div class="card-body">
<div class="table-responsive">
        <table class="table table-sm">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Updated</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dmsCategory as $dmsCategory)
                <tr>
                <td>{{$dmsCategory->name}}</td>
                <td>{{$dmsCategory->updated_at}}</td> 
                <td><a href="/delete_dms_cate/{{$dmsCategory->id}}"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></a></td>      
                </tr>
                @endforeach
            </tbody>
        </table>
</div>

    </div>
      </div>
</div>
    </div>

    <div id="newPosition" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"> Add Categorie</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
            <form class="newOrgPosition" method="post" action="/add_doc_category">
              <div class="form-group">
                <p for="inputEmail3">Category Name</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Category Name">
               </div>
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>

</section>
    <script>
      $("#save_position").on('click',function(e) {
  var form= $(".newOrgPosition");
    $.ajax({
        type: "post",
        url: "/add_system_position",
        data: form.serialize(),
        success: function(store) {
          $("#newOrgPosition").modal("hide")
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});

$("#save_employee_position").on('click',function(e) {
  var form= $(".newEmployeePositionForm");
    $.ajax({
        type: "post",
        url: "/add_employee_position",
        data: form.serialize(),
        success: function(store) {
          $("#newPosition").modal("hide")
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});
      </script>
@endsection