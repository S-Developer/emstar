@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Employess</h5>
    <span>Positions</span>
  </div>
  <div class="col-md-2">
    <div class="btn-group" role="group">
    <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#newEmployeePosition">Link Position</button>
    <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#newPosition">New Position</button>
    </div>
  </div>
  </div>
</section>
<section class="content">
    <div class="card">
    <div class="card-body">
<div class="table-responsive">
        <table class="table table-sm">
            <thead>
                <tr>
                  <th>Employee ID</th>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Created</th>
                  <th>Updated</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employeePosition as $empposition)
                <tr>
                <td>{{$empposition->emp_id}}</td>
                <td>{{$empposition->FirstName}}{{$empposition->LastName}}</td>
                <td>{{$empposition->position}}</td>
                <td>{{$empposition->created_at}}</td>
                <td>{{$empposition->updated_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>

    </div>
      </div>
    </div>

    <div id="newPosition" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"> Add Organisation Position</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
            <form class="newOrgPosition" method="post" action="/new_qualifications">
              <div class="form-group">
                <p for="inputEmail3">Position</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" id="position" name="position" placeholder="Position">
               </div>
               <div class="form-group">
                <p for="inputEmail3">Reports To</p>
                <select id="reports_to" name="reports_to" class="form-control">
                  @foreach ($positions as $poid)
                  <option value="{{$poid->id}}">{{$poid->position}}</option>
                  @endforeach
                </select>
               </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="save_position" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>




    <div id="newEmployeePosition" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"> Add Organisation Position</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
            <form class="newEmployeePositionForm" method="post" action="/new_qualifications">
              <div class="form-group">
                <p for="inputEmail3">Employee</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <select id="emp_id" name="emp_id" class="form-control">
                  @foreach ($employee as $item)
                  <option value="{{$item->emp_id}}">{{$item->FirstName}}{{$item->LastName}}</option>
                  @endforeach
                </select>
               </div>
               <div class="form-group">
                <p for="inputEmail3">Reports To</p>
                <select id="position" name="position" class="form-control">
                  @foreach ($positions as $poid)
                  <option value="{{$poid->id}}">{{$poid->position}}</option>
                  @endforeach
                </select>
               </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="save_employee_position" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>
</section>
    <script>
      $("#save_position").on('click',function(e) {
  var form= $(".newOrgPosition");
    $.ajax({
        type: "post",
        url: "/add_system_position",
        data: form.serialize(),
        success: function(store) {
          $("#newOrgPosition").modal("hide")
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});

$("#save_employee_position").on('click',function(e) {
  var form= $(".newEmployeePositionForm");
    $.ajax({
        type: "post",
        url: "/add_employee_position",
        data: form.serialize(),
        success: function(store) {
          $("#newPosition").modal("hide")
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});
      </script>
@endsection