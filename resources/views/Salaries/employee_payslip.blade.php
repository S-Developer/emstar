@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>{{$employee->FirstName}} {{$employee->LastName}}</h5>
    <span>Payslip</span>
  </div>
  <div class="col-md-2">
    <a  href="#"><Button class="btn btn-primary btn-sm">Send Payslip</Button></a>
    <a  href="#"><Button type="button" onclick="printDiv('payslip')" class="btn btn-success btn-sm">Download Payslip</Button></a>
  </div>
  </div>
</section>
<section class="content" id="payslip">
<div class="row">           
        <div class= "col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 payslip_border">
                        <div class="row">
                        <div class="col-md-4">
                            <p><b>Co.name</b>&nbsp {{$company_profile ->name}}</p>
                            <p><b>Emp Code</b>&nbsp {{$employee->emp_id}}</p>
                            <p><b>Emp.name</b>&nbsp {{$employee->title}}.{{$employee->FirstName}} {{$employee->LastName}}</p>
                            <p><b>Emp Address</b>&nbsp {{$address->address}}<br/> {{$address->town}} {{$address->city}}<br/>{{$address->country}}</p>
                        </div>
                        <div class="col-md-4"><p><b>Co.Address</b>&nbsp {{$company_profile ->name}}</p></div>
                        <div class="col-md-4"> <p><b>Payment Dt</b>&nbsp 25/02/2022</p>
                            <p><b>Dt Engaged</b>&nbsp 25/02/2022</p>
                            <p><b>Account No</b>&nbsp {{$banksInfo ->AccountNumber}}</p>
                            <p><b>Branch Code</b>&nbsp {{$banksInfo->BranchCode}}</p></div>
                        </div>

                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 ">
                        <div class="row">
                        <div class="col-md-6 payslip_earnings_border">
                            <center><h5><b>Earnings</b></h5></center>
                            <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Description</th>
      <th scope="col">Amount</th>
    </tr>
  </thead>
  <tbody>
  @foreach($salaryEarnings as $en)
                <tr>
                <td>{{$en->item}}</td>
                <td>{{$en->value}}</td>
                </tr>
@endforeach
<tr>
    <td><b>Total</b></td>
        <td>{{$totalearnings}}</td>
</tr>
</tbody>
</table>
                        </div>
                        <div class="col-md-6 payslip_earnings_border">
                        <center><h5><b>Deductions</b></h5></center>

                        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Description</th>
      <th scope="col">Amount</th>
    </tr>
  </thead>
  <tbody>
  @foreach($salaryDeduction as $sd)
                <tr>
                <td>{{$sd->item}}</td>
                <td>{{$sd->value}}</td>
                </tr>
@endforeach
<tr>
    <td><b>Total</b></td>
        <td>{{$totaldeductions}}</td>
</tr>
</tbody>
</table>
                        </div>

                    </div>
                    <div class="col-md-1"></div>
                </div>


</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10 payslip_border ">
        <div class="row">
            <div class="col-md-6"></div>
            <div class= "col-md-6 row "><h3 class= "col-md-6" >NETT PAY <b class= "col-md-6">{{$net}}</b></h3></div>
</div>
    </div>
    <div class="col-md-1"></div>
</div>
</div>
</div>
</section>
@endsection