@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>{{$employee->FirstName}} {{$employee->LastName}}</h5>
    <span>New Contract</span>
  </div>
  <div class="col-md-2">
    <a  href="#"><Button class="btn btn-primary btn-sm">Save Employee Contract</Button></a>
  </div>
  </div>
</section>
<section class="content">
       
        <form class="deductions_form">
         <div class="row">           
        <div class= "col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 for="inputEmail3">Employee Grade</h5>
                </div>
                <div class="card-body">
            <div class="form-group">
                <select id="grade_id" name="grade_id" class="form-control">
                 @foreach ($salaryGrades as $item)
                 <option value="{{$item->id}}">{{$item->value}}  {{$item->description}}</option>
                 @endforeach
               </select>
            </div>
            
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 for="inputEmail3">Employee Basic Salary</h5>
                </div>
                <div class="card-body">
            <div class="form-group">
                <input type="text" placeholder="Basic Salary" class="form-control" maxlength="50" id="item" name="item" required>
            </div>
            
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 for="inputEmail3">Salary Earnings</h5>
                </div>
                <div class="card-body">
            <div class="form-group">
               
                 @foreach ($salaryEarnings as $item)
                 <div class="form-group">
                    <div class="form-check">
                       <input type="checkbox" class="form-check-input" id="exampleCheck1">
                       <label class="form-check-label" for="exampleCheck1">{{$item->item}}  ${{$item->value}}</label>
                     </div>
                   </div>
                 @endforeach
                </div>
                </div>
            </div>
        </div>
        <div class= "col-md-6">
            <div class="form-group">
        </div>
        <div class="card">
            <div class="card-header">
                <h5 for="inputEmail3">Salary Deductions</h5>
            </div>
            <div class="card-body">
                
                 @foreach ($salaryDeduction as $item)
                 <div class="form-group">
                 <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">{{$item->item}}  ${{$item->value}}</label>
                  </div>
                </div>
                 @endforeach
                
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5 for="inputEmail3">Income Tax</h5>
        </div>
        <div class="card-body">
            
             @foreach ($taxRate as $item)
             <div class="form-group">
             <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">{{$item->item}}  {{$item->value}} %</label>
              </div>
            </div>
             @endforeach
            
    </div>
</div>
</div>
</form>
    </div>
</section>
@endsection