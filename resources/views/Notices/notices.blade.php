@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Company Notices</h5>
    <span>All Notices</span>
  </div>
  <div class="col-md-2">
  <Button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#notice_modal">New Notice</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div id="example2"></div>
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="notices_table" class="table table-sm table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>Date</th>
                <th>Notice</th>
              </tr>
          </thead>

        </table>

      </div>
    </div>
      </div>
    </div>

</section>


<div class="modal fade" id="notice_modal" tabindex="-1" role="dialog" aria-labelledby="my-modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">New Notice</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="send_u_notice">
 
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">Notice</p>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="cmp_id" value="1" id="cmp_id">
                 <input type="hidden" name="posted_by" value="1" id="posted_by">
                 <textarea  class="form-control" id="notice" name="notice"></textarea>
                </div>
 </div>

 </form>
           </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="send_notice" class="btn-sm btn btn-primary">Send Notice</button>
      </div>
    </div>
  </div>
</div>




<script>
$(document).ready(function() {
  getNotices();
});
</script>
@endSection