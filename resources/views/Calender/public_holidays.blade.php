@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Public Holidays</h5>
    <span>All Public Holidays</span>
  </div>
  <div class="col-md-2">
    <
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div id="example2"></div>
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="public_holiday_table" class="table table-sm table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>Date</th>
                <th>Holiday</th>
              </tr>
          </thead>

        </table>

      </div>
    </div>
      </div>
    </div>

</section>


<script>
$(document).ready(function() {
  getPublicHolidays();
});
</script>
@endSection