@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Company Events</h5>
    <span>All Events</span>
  </div>
  <div class="col-md-2">
 
  </div>
  </div>
</section>
<section class="content">
<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2 text-right p-3">
     <button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#events_modal">Create New Event</button></a>
    </div>
  </div>
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div id="example2"></div>
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="public_holiday_table" class="table table-sm table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>Date</th>
                <th>Event</th>
                <th>Description</th>
                <th>Start Time</th>
                <th>End Time</th>
              </tr>
          </thead>

        </table>

      </div>
    </div>
      </div>
    </div>

</section>


<div class="modal fade" id="events_modal" tabindex="-1" role="dialog" aria-labelledby="my-modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Record Vistor Time Out</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="record_time_out">
 
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">Event Name</p>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="cmp_id" value="1" id="cmp_id">
                   <input type="text" class="form-control" id="name" name="name" placeholder="Event Name" required>
                </div>
 </div>
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">Description</p>
                   <input type="text" class="form-control" id="description" name="description" placeholder="Description" required>
                </div>
 </div>
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">Date</p>
                   <input type="date" class="form-control" id="date" name="date" placeholder="Date" required>
                </div>
 </div>
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">Start Time</p>
                   <input type="time" class="form-control" id="start_time" name="start_time" placeholder="Time Out" required>
                </div>
 </div>
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">End Time</p>
                   <input type="time" class="form-control" id="end_time" name="end_time" placeholder="End Time" required>
                </div>
 </div>
 </form>
           </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="save_event" class="btn-sm btn btn-primary">Save Event</button>
      </div>
    </div>
  </div>
</div>




<script>
$(document).ready(function() {
  getEvents();
});
</script>
@endSection