<div id="address">
    <div class="card">
      <div class="card-header">
          <div class="row ">
               <div class="col-md-10">
                  Bank Details
               </div>
          <div class="col-md-2">
              <Button type="button" id="updateBankDetails" class="btn btn-sm btn-primary">Save Changes</Button>
          </div>
      </div>
      </div>
      <div class="card-body">
        <form class="bank_details">
      <div class="row">
          <div class="col-md-6">
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Account Number</p>
                  <div class="col-sm-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" value="{{$banksInfo->AccountNumber}}" class="form-control" id="AccountNumber" name="AccountNumber" placeholder="Account Number">
                  </div>
                </div>

                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Account Name</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$banksInfo->AccountName}}" id="AccountName" name="AccountName" placeholder="Account Name">
                  </div>
                </div>
            </div>

            <div class="col-md-6">
              <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Bank Name</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$banksInfo->emp_id}}" id="emp_id" name="emp_id" hidden>
                    <input type="text" class="form-control"  value="{{$banksInfo->BankName}}" id="BankName" name="BankName" placeholder="BankName">
                  </div>
                </div>
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Branch Name</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$banksInfo->BranchName}}" id="BranchName" name="BranchName" placeholder="BranchName">
                  </div>
                </div>
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Branch Code</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$banksInfo->BranchCode}}" id="BranchCode" name="BranchCode" placeholder="BranchCode">
                  </div>
                </div>

          </div>
      </div>
        </form>

 </div>

 <script>
  $("#updateBankDetails").on('click',function(e) {
    var form= $(".bank_details");
      $.ajax({
          type: "post",
          url: "/update_bank_details",
          data: form.serialize(),
          success: function(store) {
            $('.toast').toast('show')
          },
          error: function(e) {
            alert(e.Message)
          }
      });
  });
     </script>