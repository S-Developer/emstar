<div id="address">
    <div class="card">
      <div class="card-header">
          <div class="row ">
               <div class="col-md-10">
                  Personal Details
               </div>
          <div class="col-md-2">
              <Button class="btn btn-sm btn-primary" type="button" id="save_prs_details">Save Personal Details</Button>
          </div>
      </div>
      </div>
      <div class="card-body">
    <form class="employeePersonal"> 
      <div class="row">
          <div class="col-md-6">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 ">Title</p>
                <div class="col-sm-8">
              <select id="title"  value="{{$employee->title}}" name="title" class="custom-select">
              <option value="Mr">Mr</option>
              <option value="Mrs">Mrs</option>
              <option value="Miss">Miss</option>
            </select>
                    </div>
              </div>

              <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Firstname</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$employee->emp_id}}" id="emp_id" name="emp_id" hidden>
                    <input type="text" class="form-control" value="{{$employee->FirstName}}" id="firstname" name="firstname" placeholder="firstname">
                  </div>
                </div>
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Middlename</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$employee->MiddleName}}" id="middlename" name="middlename" placeholder="middlename">
                  </div>
                </div>


                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Lastname</p>
                  <div class="col-sm-8">
                    <input type="text" value="{{$employee->LastName}}"  class="form-control" id="lastname" name="lastname" placeholder="lastname">
                  </div>
                </div>
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Gender</p>
                  <div class="col-sm-8">
                <select id="gender" value="{{$employee->Gender}}"  name="gender" class="custom-select">
                <option value="male">Male</option>
                <option value="female">Female</option>
                <option value="">Rather not to say</option>
              </select>
                      </div>
                </div>
          </div>
          <div class="col-md-6">
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Status</p>
                  <div class="col-sm-8">
                <select id="status" value="{{$employee->Status}}"  name="status" class="custom-select">
                <option value="Single">Single</option>
                <option value="Married">Married</option>
                <option value="Divorced">Divorced</option>
              </select>
                      </div>
                </div>

                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Date of Birth</p>
                  <div class="col-sm-8">
                    <input type="date" value="{{$employee->DateOfBirth}}" class="form-control" id="dob" name="dob" placeholder="Date of birth">
                  </div>
                </div>

                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">ID Number</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$employee->IdNumber}}" id="idnumber" name="idnumber" placeholder="National ID Number">
                  </div>
                </div>
                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Phone Number</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$employee->PhoneNumber}}" id="phone" name="phone" placeholder="Phone Number">
                  </div>
                </div>

                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">Email Address</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$employee->Email}}" id="email" name="email" placeholder="Email Address">
                  </div>
                </div>

          </div>
      </div>
    </form>
 </div>
    </div>
</div>


 <div id="address">
  <div class="card">
    <div class="card-header">
        <div class="row ">
             <div class="col-md-10">
                Next of Kin Details
             </div>
        <div class="col-md-2">
            <Button class="btn btn-sm btn-primary" type="button" id="save_next_of_kin_details">Save Next of Kin Details</Button>
        </div>
    </div>
    </div>
    <div class="card-body">
  <form class="nextOfkin">
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group row">
              <p for="inputEmail3" class="col-sm-4 ">Title</p>
              <div class="col-sm-8">
            <select id="N_title"  value="{{$nextOfKin->N_title}}" name="N_title" class="custom-select">
            <option value="Mr">Mr</option>
            <option value="Mrs">Mrs</option>
            <option value="Miss">Miss</option>
          </select>
                  </div>
            </div>

            <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 col-form-label">Firstname</p>
                <div class="col-sm-8">
                  <input type="text" class="form-control" value="{{$nextOfKin->emp_id}}" id="emp_id" name="emp_id" hidden>
                  <input type="text" class="form-control" value="{{$nextOfKin->First_Name}}" id="First_Name" name="First_Name" placeholder="firstname">
                </div>
              </div>


              <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 col-form-label">Lastname</p>
                <div class="col-sm-8">
                  <input type="text" value="{{$nextOfKin->Last_Name}}"  class="form-control" id="Last_Name" name="Last_Name" placeholder="lastname">
                </div>
              </div>

        </div>
        <div class="col-md-6">

              <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 col-form-label">ID Number</p>
                <div class="col-sm-8">
                  <input type="text" class="form-control" value="{{$nextOfKin->Id_Number}}" id="Id_Number" name="Id_Number" placeholder="National ID Number">
                </div>
              </div>
              <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 col-form-label">Phone Number</p>
                <div class="col-sm-8">
                  <input type="text" class="form-control" value="{{$nextOfKin->Phone_Number}}" id="Phone_Number" name="Phone_Number" placeholder="Phone Number">
                </div>
              </div>

              <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 col-form-label">Email Address</p>
                <div class="col-sm-8">
                  <input type="text" class="form-control" value="{{$nextOfKin->N_Email}}" id="N_Email" name="N_Email" placeholder="Email Address">
                </div>
              </div>

              <div class="form-group row">
                <p for="inputEmail3" class="col-sm-4 col-form-label">Relationship</p>
                <div class="col-sm-8">
                  <input type="text" class="form-control" value="{{$nextOfKin->Relationship}}" id="Relationship" name="Relationship" placeholder="Relationship">
                </div>
              </div>

        </div>
    </div>
  </form>
</div>
 <script>
$("#save_prs_details").on('click',function(e) {
  var form= $(".employeePersonal");
    $.ajax({
        type: "post",
        url: "/update_personal_details",
        data: form.serialize(),
        success: function(store) {
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});

$("#save_next_of_kin_details").on('click',function(e) {
  var form= $(".nextOfkin");
    $.ajax({
        type: "post",
        url: "/update_next_of_kin_details",
        data: form.serialize(),
        success: function(store) {
          $('.toast').toast('show')
        },
        error: function(e) {
          alert(e.Message)
        }
    });
});
   </script>