@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Employees Database</h5>
    <span>All Registered Employees</span>
  </div>
  <div class="col-md-2">

  </div>
</section>
<section class="content">
    <div class="card">
        <form method="post" action="/new_employee">
        <div class="card-body">
      <div class="row">


          <div class="col-md-6">
            <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 ">Title</p>
                <div class="col-sm-10">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <select id="title" name="title" class="custom-select">
               <option value="Mr">Mr</option>
               <option value="Mrs">Mrs</option>
               <option value="Miss">Miss</option>
               </select>
                    </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Firstname</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="firstname" name="firstname" placeholder="firstname">
                </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Middlename</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="middlename" name="middlename" placeholder="middlename">
                </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Lastname</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="lastname" name="lastname" placeholder="lastname">
                </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Gender</p>
                <div class="col-sm-10">
               <select id="gender" name="gender" class="custom-select">
               <option value="male">Male</option>
               <option value="female">Female</option>
               <option value="">Rather not to say</option>
               </select>
                    </div>
          </div>
          </div>
          <div class="col-md-6">

            <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Status</p>
                <div class="col-sm-10">
               <select id="status" name="status" class="custom-select">
               <option value="Single">Single</option>
               <option value="Married">Married</option>
               <option value="Divorced">Divorced</option>
               </select>
                    </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Date of Birth</p>
                <div class="col-sm-10">
                  <input type="date" class="form-control" id="dob" name="dob" placeholder="Date of birth">
                </div>
               </div>
               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">ID Number</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="idnumber" name="idnumber" placeholder="National ID Number">
                </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Phone Number</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="phone">
                </div>
               </div>


               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Email Address</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email Address">
                </div>
               </div>

          </div>


      </div>
      <div class="form-group row">
        <div class="col-sm-10">
         <Button type="submit" class="btn btn-sm btn-primary">Save Employee</Button>
        </div>
    </div>
    </div>


      </div>
    </div>
</form>
</section>
@endsection