@extends('MainLayouts/main')
@section('content')
<style>
    .emstar_tab{
        background-color:#dadada;
        margin: 5px;
        border-radius: 10px;
        color: inherit;

    }
    .activ{
        background-color:#343a40;
        color: white;
        border-radius: 10px;
        text-decoration-color: #dadada

    }
     a :hover{
        background:none;

    }
    </style>
   

    <section class="content-header">
        <div class="row">
        <div class="col-md-12">
          <h5>{{$firstname}} {{$lastname}}</h5>
          <span>Employee Info@</span>
        </div>
        <form class="employee_detail">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" id="emp_id" name="emp_id" value="{{$emp_id}}"/>
        </form>

      </section>
      <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header_tabs">
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item emstar_tab">
                      <a class="nav-link activ" id="overview" onclick="loadPage('/personal_details')" href="#">Overview</a>
                    </li>
                    <li class="nav-item emstar_tab">
                      <a class="nav-link" id="tab" onclick="loadPage('/address_details')" href="#">Address Details</a>
                    </li>
                    <li class="nav-item emstar_tab">
                      <a class="nav-link" id="tab" onclick="loadPage('/bank_details')"   href="#">Bank Details</a>
                    </li>
                    <li class="nav-item emstar_tab">
                        <a class="nav-link" id="tab" onclick="loadPage('/employee_qualifications')"   href="#">Qualifications</a>
                    </li>
                    <li class="nav-item emstar_tab">
                        <a class="nav-link" id="tab" onclick="loadPage('/documents_details')"  href="#">Documents</a>
                    </li>
                    <li class="nav-item emstar_tab">
                        <a class="nav-link" id="tab" onclick="loadPage('/notes_details')"  href="#">Notes</a>
                    </li>
                  </ul>
            </div>
                </div>

            </div>

        </div>
      </section>
      <section class="content">
          <div class="row">
              <div class="col-md-12" id="content_div">

             </div>
          </div>
      </section>
      <script>
  function loadPage(values){
    var form= $(".employee_detail");
      $.ajax({
        type: "post",
        url: values,
        data: form.serialize(),
        success: function(data) {
          $('#content_div').html(data);
        },
        error: function(e) {
          alert(e.Message)
        }
      });
           }
           $('.nav-link').on('click',function(){
            $('.nav-link').removeClass('activ');
            $(this).addClass('activ');
          });

    $(document).ready(function(){
      var form= $(".employee_detail");
      $.ajax({
        type: "post",
        url: "/personal_details",
        data: form.serialize(),
        success: function(data) {
          $('#content_div').html(data);
        },
        error: function(e) {
          alert(e.Message)
        }
      });

          });



      </script>
@endsection