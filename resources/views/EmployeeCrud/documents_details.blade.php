<div id="address">
  <div class="card">
    <div class="card-header">
        <div class="row ">
             <div class="col-md-10">
               Employee Documents
             </div>
        <div class="col-md-2">
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add New</button>
        </div>
    </div>
    </div>
    <div class="card-body">
    <div class="row">
        <table class="table table-sm">
            <thead class="table-dark">
                 <td></td>
                  <td>Title</td>
                  <td>Description</td>
                  <td>Added On</td>
                  <td></td>
            </thead>
            <tbody>
                @foreach($employeeDocuments as $documents)
                <tr>
                  <td><i class="fa fa-bookmark-o"></i></td>
                <td>{{$documents->title}}</td>
                <td>{{$documents->description}}</td>
                <td>{{$documents->created_at}}</td>
                <td><a  target="_blank" href="http://localhost:8000/Documents/{{$documents->document_path}}"><button class="btn btn-sm btn-danger">Open</button></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>

      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Document</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
              <form method="POST" enctype="multipart/form-data" id="upload_image_form" action="javascript:void(0)" >

                <div class="row">
                    <div class="col-md-12 mb-2">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 mb-2">
                    <div class="form-group">
                      <p for="inputEmail3">Title</p>
                       <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 mb-2">
                    <div class="form-group">
                      <p for="inputEmail3">Description</p>
                       <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" id="emp_id" name="emp_id" value="{{$data['emp_id']}}" hidden/>
                            <input type="file" name="image" placeholder="Choose image" id="image">
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                    </div>
                  </div>



                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>

        </div>
      </div>
    </div>

</div>

<script>
 $('#upload_image_form').submit(function(e) {

e.preventDefault();

var formData = new FormData(this);

$.ajax({
   type:'POST',
   url: "{{ url('save_documents')}}",
   data: formData,
   cache:false,
   contentType: false,
   processData: false,
   success: (data) => {
      this.reset();
      alert('Image has been uploaded successfully');
   },
   error: function(data){
      console.log(data);
    }
  });
});

</script>