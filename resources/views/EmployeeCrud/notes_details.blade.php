<div id="address">
  <div class="card">
    <div class="card-header">
        <div class="row ">
             <div class="col-md-10">
               Employee Qualifications
             </div>
        <div class="col-md-2">
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add New</button>
        </div>
    </div>
    </div>
    <div class="card-body">
    <div class="row">
        <table class="table table-sm">
            <thead class="table-dark">
                  <td>Notes</td>
                  <td>Added On</td>
            </thead>
            <tbody>
                @foreach($employeeNotes as $employeeNote)
                <tr>
                <td>{{$employeeNote->notes}}</td>
                <td>{{$employeeNote->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>




      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Notes</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
              <form class="newNotes" method="post" action="/new_notes">
                <div class="form-group">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" id="emp_id" name="emp_id" value="{{$data['emp_id']}}" hidden/>

                </div>
                 <div class="form-group">
                   <p for="inputEmail3">Notes</p>
                   <Textarea class="form-control" id="notes" name="notes"></Textarea>
                 </div>


            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="addNotes" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>

        </div>
      </div>
    </div>
</div>
  </div>
  <script>
$("#addNotes").on('click',function(e) {
var form= $(".newNotes");
  $.ajax({
      type: "post",
      url: "/add_employee_notes",
      data: form.serialize(),
      success: function(store) {
        $('.myModal').hide();
        $('.toast').toast('show');
      },
      error: function(e) {
        alert(e.Message)
      }
  });
});
     </script>