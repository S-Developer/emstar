<div id="address">
    <div class="card">
      <div class="card-header">
          <div class="row ">
               <div class="col-md-10">
                  Address Details
               </div>
          <div class="col-md-2">
              <Button type="button" id="updateAddressDetails" class="btn btn-sm btn-primary">Save Address Details</Button>
          </div>
      </div>
      </div>
      <div class="card-body">
        <form class="addressDetails">
      <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <p for="inputEmail3" class="col-sm-4 col-form-label">Address</p>
              <div class="col-sm-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="text" class="form-control" value="{{$address->emp_id}}" id="emp_id" name="emp_id" hidden>
                <input type="text" class="form-control" value="{{$address->address}}" id="address" name="address" placeholder="address">
              </div>
            </div>
              <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">town</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$address->town}}" id="town" name="town" placeholder="town">
                  </div>
                </div>



          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <p for="inputEmail3" class="col-sm-4 col-form-label">City</p>
              <div class="col-sm-8">
                <input type="text" class="form-control" value="{{$address->city}}" id="city" name="city" placeholder="city">
              </div>
            </div>

                <div class="form-group row">
                  <p for="inputEmail3" class="col-sm-4 col-form-label">County</p>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$address->county}}"  id="county" name="county" placeholder="county">
                  </div>
                </div>


          </div>
        </form>
      </div>

 </div>
 <script>
  $("#updateAddressDetails").on('click',function(e) {
    var form= $(".addressDetails");
      $.ajax({
          type: "post",
          url: "/update_address_details",
          data: form.serialize(),
          success: function(store) {
            $('.toast').toast('show')
          },
          error: function(e) {
            alert(e.Message)
          }
      });
  });
     </script>



