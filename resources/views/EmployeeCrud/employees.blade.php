@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Employees Database</h5>
    <span>All Registered Employees</span>
  </div>
  <div class="col-md-2">
    <a href="/new_employee"><Button class="btn btn-primary btn-sm">Create new Employee</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="userstable" class="table table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>File#</th>
                <th>Title</th>
                <th>First Name</th>
                <th>Lastname</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th></th>
              </tr>
          </thead>

        </table>
      </div>
    </div>
      </div>
    </div>
    <script>
    $(document).ready(function() {
      getEmployees();
    });
    function getEmployees() {
      $.ajax({
          type: "GET",
          contentType: "application/json",
          dataType: "json",
          url: "/employees",
          success: function(response) {

              $('#userstable').DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],

                  "data": response,
                  "columns": [{
                          "data": "emp_id"
                      },
                      {
                          "data": "title"
                      },
                      {
                          "data": "FirstName"
                      },
                      {
                          "data": "LastName"
                      },
                      {
                          "data": "PhoneNumber"
                      },
                      {
                          "data": "Email"
                      },

                      {
                          "data": "id",
                          "render": function(data, type, row) {
                              return ' <a href="/employees_details/'+row.emp_id+','+row.FirstName+','+row.LastName+'" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>&nbsp<a href="/employee_profile/'+row.emp_id+','+row.FirstName+','+row.LastName+'" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Profile"><i class="fa fa-user"></i></a>&nbsp<a href="#" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Calender"><i class="fa fa-calendar"></i></a>&nbsp<a href="employee_new_contract/'+row.emp_id+'" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Contract"><i class="fa fa-file-contract"></i></a>&nbsp<a href="employee_payslip/'+row.emp_id+'" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Payslip"><i class="fa fa-file"></i></a>'
                              }
                      }

                  ]
              }).buttons().container().appendTo( $('#example') );
          }

      });
  }
  </script>

</section>
@endSection