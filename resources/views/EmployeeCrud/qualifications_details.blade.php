<div id="address">
    <div class="card">
      <div class="card-header">
          <div class="row ">
               <div class="col-md-10">
                 Employee Qualifications
               </div>
          <div class="col-md-2">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add New</button>
          </div>
      </div>
      </div>
      <div class="card-body">
      <div class="row">
          <table class="table table-sm">
              <thead class="table-dark">
                    <td>Qualification</td>
                    <td>Description</td>
                    <td>Year Attained</td>
                    <td>Institution</td>
                    <td>Added On</td>
              </thead>
              <tbody>
                  @foreach($my_qualifications as $qualification)
                  <tr>
                  <td>{{$qualification->qualification_type}}</td>
                  <td>{{$qualification->description}}</td>
                  <td>{{$qualification->year_attained}}</td>
                  <td>{{$qualification->institution}}</td>
                  <td>{{$qualification->created_at}}</td>
                  </tr>
                  @endforeach
              </tbody>
          </table>




        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Add Qualification</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

              </div>
              <div class="modal-body">
                <form class="newQualifications" method="post" action="/new_qualifications">
                  <div class="form-group">
                    <p for="inputEmail3">Qualification</p>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" id="emp_id" name="emp_id" value="{{$data['emp_id']}}" hidden/>
                    <select id="qualification_type" name="qualification_type" class="form-control">
                    @foreach ($qualificationsTypes as $qualificationsType)
                      <option value="{{$qualificationsType->qualification_type}}">{{$qualificationsType->qualification_type}}</option>
                    @endforeach
                    </select>
                  </div>

                   <div class="form-group">
                     <p for="inputEmail3">Description</p>
                      <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                   </div>

                   <div class="form-group">
                    <p for="inputEmail3">Institution</p>
                      <input type="text" class="form-control" id="institution" name="institution" placeholder="institution">
                   </div>

                   <div class="form-group">
                    <p for="inputEmail3">Year attained</p>
                      <input type="date" class="form-control" id="year_attained" name="year_attained" placeholder="Year Attained">
                   </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="addQualification" class="btn btn-primary">Save</button>
              </div>
            </div>
          </form>

          </div>
        </div>
      </div>
 </div>
    </div>

 <div id="address">
  <div class="card">
    <div class="card-header">
        <div class="row ">
             <div class="col-md-10">
               Employee Experience
             </div>
        <div class="col-md-2">
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#experienceModal">Add New</button>
        </div>
    </div>
    </div>
    <div class="card-body">
    <div class="row">
        <table class="table table-sm">
            <thead class="table-dark">
                  <td>Company</td>
                  <td>Position</td>
                  <td>Description </td>
                  <td>Employed From </td>
                  <td>To</td>
            </thead>
            <tbody>
                @foreach($my_experience as $experience)
                <tr>
                <td>{{$experience->company}}</td>
                <td>{{$experience->position}}</td>
                <td>{{$experience->description}}</td>
                <td>{{$experience->from_datetime}}</td>
                <td>{{$experience->to_datetime}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>




      <div id="experienceModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Experience</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
              <form class="newExperience" method="post" action="/new_experience">
                <div class="form-group">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" id="emp_id" name="emp_id" value="{{$data['emp_id']}}" hidden/>

                </div>

                 <div class="form-group">
                   <p for="inputEmail3">Company</p>
                    <input type="text" class="form-control" id="company" name="company" placeholder="Company">
                 </div>

                 <div class="form-group">
                  <p for="inputEmail3">Position</p>
                    <input type="text" class="form-control" id="position" name="position" placeholder="position">
                 </div>

                 <div class="form-group">
                  <p for="inputEmail3">Description</p>
                    <input type="text" class="form-control" id="description" name="description" placeholder="description">
                 </div>

                 <div class="form-group">
                  <p for="inputEmail3">Date Started</p>
                    <input type="date" class="form-control" id="from_datetime" name="from_datetime" placeholder="Year Attained">
                 </div>
                 <div class="form-group">
                  <p for="inputEmail3">Date Contract Closed</p>
                    <input type="date" class="form-control" id="to_datetime" name="to_datetime" placeholder="Year Attained">
                 </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="addExperience" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>

        </div>
      </div>
    </div>

</div>


 <script>
  $("#addQualification").on('click',function(e) {

    var form= $(".newQualifications");
      $.ajax({
          type: "post",
          url: "/add_employee_qualifications",
          data: form.serialize(),
          success: function(store) {
            $('.myModal').hide();
            $('.toast').toast('show');
          },
          error: function(e) {
            alert(e.Message)
          }
      });
  });


  $("#addExperience").on('click',function(e) {

var form= $(".newExperience");
  $.ajax({
      type: "post",
      url: "/add_employee_experience",
      data: form.serialize(),
      success: function(store) {
        $('.myModal').hide();
        $('.toast').toast('show');
      },
      error: function(e) {
        alert(e.Message)
      }
  });
});
     </script>