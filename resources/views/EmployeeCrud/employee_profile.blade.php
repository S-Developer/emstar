@extends('MainLayouts/main')
@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Profile</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Employees</a></li>
            <li class="breadcrumb-item active">User Profile</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/user4-128x128.jpg"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$employees->FirstName}} {{$employees->LastName}}</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>DOB</b> <a class="float-right">{{$employees->DateOfBirth}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right">{{$employees->PhoneNumber}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$employees->Email}}</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Bank Details</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Address Details</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Next of Kin Details</a></li>
                  <li class="nav-item"><a class="nav-link" href="#documents" data-toggle="tab">Documents</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                   <div class="row">
                    <div class="col-md-8">
                    <div class="callout callout-info">
                        <ul class="list-group list-group-unbordered mb-6">
                            <li class="list-group-item">
                              <b>Account Name</b> <a class="float-right">{{$bank->AccountName}}</a>
                            </li>
                            <li class="list-group-item">
                              <b>Account Number</b> <a class="float-right">{{$bank->AccountNumber}}</a>
                            </li>
                            <li class="list-group-item">
                              <b>Bank Name</b> <a class="float-right">{{$bank->AccountNumber}}</a>
                            </li>
                            <li class="list-group-item">
                              <b>Branch Name</b> <a class="float-right">{{$bank->BranchName}}</a>
                            </li>
                            <li class="list-group-item">
                              <b>Branch Code></b> <a class="float-right">{{$bank->BranchCode}}</a>
                            </li>
                          </ul>
                    </div>
                    </div>
                   </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">

                    <div class="row">
                        <div class="col-md-8">
                        <div class="callout callout-info">
                            <ul class="list-group list-group-unbordered mb-6">
                                <li class="list-group-item">
                                  <b>Address</b> <a class="float-right">{{$address->address}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>Town</b> <a class="float-right">{{$address->town}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>City</b> <a class="float-right">{{$address->city}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>Country</b><a class="float-right">{{$address->county}}</a>
                                </li>
                              </ul>
                        </div>
                        </div>
                       </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="settings">
                    <div class="col-md-8">
                        <div class="callout callout-info">
                            <ul class="list-group list-group-unbordered mb-6">
                                <li class="list-group-item">
                                  <b>Name</b> <a class="float-right">{{$nextOfKin->N_title}} {{$nextOfKin->First_Name}} {{$nextOfKin->Last_Name}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>Phone</b> <a class="float-right">{{$nextOfKin->Phone_Number}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>National ID</b> <a class="float-right">{{$nextOfKin->Id_Number}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>Email</b> <a class="float-right">{{$nextOfKin->N_Email}}</a>
                                </li>
                                <li class="list-group-item">
                                  <b>Relationship</b> <a class="float-right">{{$nextOfKin->Relationship}}</a>
                                </li>
                              </ul>
                        </div>
                        </div>
                       </div>
                  <div class="tab-pane" id="documents">
                    <div class="card card-info">
                        <div class="card-header">
                          <h3 class="card-title">Files</h3>

                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                              <i class="fas fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="card-body p-0">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>File Name</th>
                                <th>File Size</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($employeeDocuments as $document )
                                <tr>
                                <td>{{$document->title}}</td>
                                <td class="text-right py-0 align-middle">
                                  <div class="btn-group btn-group-sm">
                                    <a  target="_blank" href="http://localhost:8000/Documents/{{$document->document_path}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                  </div>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                  </div>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        <!-- /.row -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">Qualifications</h3>

                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                    </div>
                    <div class="card-body p-0">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Institutiom</th>
                            <th>Year Attained</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($my_qualifications as $qualifications)
                        <tr>
                            <td>{{$qualifications->qualification_type}}</td>
                            <td>{{$qualifications->institution}}</td>
                            <td>{{$qualifications->year_attained}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                      <h3 class="card-title">Experience</h3>

                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                      </div>
                    </div>
                    <div class="card-body p-0">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Company</th>
                            <th>Position</th>
                            <th>From Date </th>
                            <th>To Date</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>

                          <tr>
                              @foreach ($my_experience as $experience)
                              <td>{{$experience->company}}</td>
                              <td>{{$experience->position}}</td>
                              <td>{{$experience->from_datetime}}</td>
                              <td>{{$experience->to_datetime}}</td>
                              @endforeach


                          </tr>


                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</section>

@endsection