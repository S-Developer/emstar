@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>DMS</h5>
    <span>Create New</span>
  </div>
  <div class="col-md-2">

  </div>
</section>
<section class="content">
    <div class="card">
        <form method="post" action="/save_dms" enctype="multipart/form-data" >
        <div class="card-body">
      <div class="row">

          <div class="col-md-6">
            <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 ">Category</p>
                <div class="col-sm-10">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <select id="category_id" name="category_id" class="custom-select">
               @foreach($dmsCategory as $dmsCategory)
               <option value="{{$dmsCategory->id}}">{{$dmsCategory->name}}</option>
                @endforeach
               </select>
                    </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">name</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Title">
                </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Description</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="description">
                </div>
               </div>

          </div>
          <div class="col-md-6">

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Searchparams</p>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search keys">
                </div>
               </div>

               <div class="form-group row">
                <p for="inputEmail3" class="col-sm-2 col-form-label">Document</p>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="url" name="url" placeholder="">
                </div>
               </div>
          </div>


      </div>
      <div class="form-group row">
        <div class="col-sm-10">
         <Button type="submit" class="btn btn-sm btn-primary">Save to Dms</Button>
        </div>
    </div>
    </div>


      </div>
    </div>
</form>
</section>
@endsection