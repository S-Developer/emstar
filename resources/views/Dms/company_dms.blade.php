@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Our DMS</h5>
    <span>Documents</span>
  </div>
  <div class="col-md-2">
    <div class="btn-group" role="group">
    
    </div>
  </div>
  </div>
</section>
<section class="content">
  <div class="col-md-12">
  <div class="card">
    <div class="card-body">
<div class="table-responsive">
        <table  id="dms_table" class="table table-sm">
            <thead>
                <tr>
                  <th>Categorie</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Search Params</th>
                  <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
</div>

    </div>
      </div>
</div>
    </div>

</section>
<script>
    $(document).ready(function(){
    getDms();
    });
</script>
 
@endsection