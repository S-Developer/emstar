@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
  <h5>Appointments</h5>
    <span>Calendar</span>
  </div>
  <div class="col-md-2">
  </div>
  </div>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2 text-right p-3">
      <a href="{{'/new_appointments'}}"><Button class="btn btn-sm btn-primary" >Create New Appointment</button></a>
    </div>
  </div>
    <div class="row">
  <div class="col-md-12">
    <div class="card">
    <div id='calendar'></div>
  </div>
</div>
    </div>
</section>
<script>
document.addEventListener('DOMContentLoaded', function() {
	var calendarEl = document.getElementById('calendar');
	var calendar = new FullCalendar.Calendar(calendarEl, {
		headerToolbar: {
			left: 'prevYear,prev,next,nextYear today',
			center: 'title',
			right: 'dayGridMonth,dayGridWeek,dayGridDay'
		},
		initialDate: new Date(),
		navLinks: true, // can click day/week names to navigate views
		editable: true,
		dayMaxEvents: true, // allow "more" link when too many events
		events: "/appointments"

	});
    calendar.render();

});
</script>
@endSection
