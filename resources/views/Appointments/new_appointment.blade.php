@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Employees Database</h5>
    <span>Search for Employee</span>
  </div>
  <div class="col-md-2">
    
  </div>
  </div>
</section>
<section class="content">
<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2 text-right p-3">
      <a href="{{ URL::previous() }}"><button class="btn btn-sm btn-primary" >Go Back</button></a>
    </div>
  </div>
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="userstable" class="table table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>File#</th>
                <th>Title</th>
                <th>First Name</th>
                <th>Lastname</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th></th>
              </tr>
          </thead>

        </table>
      </div>
    </div>
      </div>
    </div>
    <script>
    $(document).ready(function() {
      getEmployees();
    });
    function getEmployees() {
      $.ajax({
          type: "GET",
          contentType: "application/json",
          dataType: "json",
          url: "/all_employees",
          success: function(response) {

              $('#userstable').DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],

                  "data": response,
                  "columns": [{
                          "data": "emp_id"
                      },
                      {
                          "data": "title"
                      },
                      {
                          "data": "FirstName"
                      },
                      {
                          "data": "LastName"
                      },
                      {
                          "data": "PhoneNumber"
                      },
                      {
                          "data": "Email"
                      },

                      {
                          "data": "id",
                          "render": function(data, type, row) {
                              return ' <a data-id="'+row.emp_id+'" data-name="'+row.FirstName+'" data-surname="'+row.LastName+'" class="btn btn-primary btn-sm create_appointment" data-toggle="modal" href="#myModal">Create Appointment</a>'
                              }
                      }

                  ]
              }).buttons().container().appendTo( $('#example') );
          }

      });
  }


  $(document).on("click", ".create_appointment", function () {
     var emp_id = $(this).data('id');
     var name = $(this).data('name');
     var surname = $(this).data('surname');
     $(".the-modal #emp_id").val( emp_id );
     $(".the-modal #name").val( name );
     $(".the-modal #surname").val( surname );

     document.getElementById("name").innerHTML = name;
     document.getElementById("surname").innerHTML = surname;
});
  </script>
<style>
  .inline_style{
    display:inline-block;
  }
  </style>
<div id="myModal" class="modal fade the-modal " role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">New Appointment for: &nbsp;<div class="inline_style" id="name"></div> &nbsp;<div class="inline_style" id="surname"></div></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>
          <div class="modal-body">
            <form class="appointment" method="post" action="/save_appointment">
            <div class="form-group">
                <label for="inputEmail3">Appointment Title</label>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" id="title" name="title" placeholder="Appointment Title">
                  <input type="hidden" name="emp_id" id="emp_id" value=""/>
                  <input type="hidden" name="empName" id="name" value=""/>
                  <input type="hidden" name="empSurname" id="surname" value=""/>
               </div>
               <p>Person Details</p>
               <div class="form-group">
                <label for="inputEmail3">Full Name</label>
                  <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Fullname">
               </div>
               <div class="form-group">
                <label for="inputEmail3">Phone Number</label>
                  <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="phone Number">
               </div>
               <div class="form-group">
                <label for="inputEmail3">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
               </div>
               <div class="row">
                   <div class="col-md-6">
                   <div class="form-group">
                <label for="inputEmail3">Date</label>
                  <input type="date" class="form-control" id="date" name="date" placeholder="Appointment Date">
               </div>
                   </div>
                   <div class="col-md-6">
                   <div class="form-group">
                <label for="inputEmail3">Time</label>
                  <input type="time" class="form-control" id="time" name="time" placeholder="Appointment Time">
               </div>
                   </div>
               </div>

               <div class="form-group">
                <label for="inputEmail3">For how long</label>
                 <select class="form-control" name="time_range_id">
                 @foreach($time_range as $time_range)
                 <option value="{{$time_range ->id}}">{{$time_range->name}}</option>

                 @endforeach
</select>
               </div>
              
               <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" name="defaultCheck1" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Is Virtual
                </label>
                </div>
              <div id="virtual_link">
              <div class="form-group">
                <label for="inputEmail3">Meeting Link</label>
                  <input type="url" class="form-control" id="url" name="url" placeholder="Meeting Url">
               </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>

</section>
<script>
  $("#virtual_link").hide();
var checkbox = document.querySelector("input[name=defaultCheck1]");
checkbox.addEventListener('change', function() {
  if (this.checked) {
    $("#virtual_link").show();
  } else {
   
    $("#virtual_link").hide();
  }
});
</script>
@endSection