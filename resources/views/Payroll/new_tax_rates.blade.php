@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Tax Rates</h5>
    <span>New Item</span>
  </div>
  <div class="col-md-2">
  <a  href="{{'/tax'}}"><Button class="btn btn-primary btn-sm">Back to Taxes</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form class="tax_form">
                <div class="form-group">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>

                 <div class="form-group">
                  <p for="inputEmail3">Item Name</p>
                    <input type="text" class="form-control" maxlength="50" id="item" name="item" required>
                 </div>
               
                 <div class="form-group">
                  <p for="inputEmail3">Value(%)</p>
                    <input type="number" class="form-control" id="currency_value" name="currency_value" required>
                 </div>
                    <div class="form-group">
                        <button type="button" id="save_tax" class="btn btn-primary">Save</button>
                     </div>
              </div>
            </div>
        </div>

  </div>
    </div>
</section>
@endsection