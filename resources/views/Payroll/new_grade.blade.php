@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Grading</h5>
    <span>Add New Grade</span>
  </div>
  <div class="col-md-2">
    <a  href="{{'/grades'}}"><Button class="btn btn-primary btn-sm">Back to EmployeedGrades</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form class="grade_form">
                <div class="form-group">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <p for="inputEmail3">Name</p>
                  <input type="text" class="form-control" maxlength="50" id="grade" name="grade" required>
                  </div>
                 <div class="form-group">
                  <p for="inputEmail3">Description</p>
                    <input type="text" class="form-control" maxlength="50" id="description" name="description" required>
                 </div>

                    <div class="form-group">
                        <button type="button" id="save_grade" class="btn btn-primary">Save</button>
                     </div>

              </div>


            </div>
        </div>

  </div>
    </div>
</section>
@endsection