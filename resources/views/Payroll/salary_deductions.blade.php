@extends('MainLayouts/main')
@section('content')

<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Deductions Database</h5>
    <span>All Deductions</span>
  </div>
  <div class="col-md-2">
    <a href="{{'/new_deductions'}}" class="nav-link"><Button class="btn btn-primary btn-sm"><i class="nav-icon fas fa-calendar-alt"></i> Add Salary Deductions</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div id="example2"></div>
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="leave_application_table" class="table table-sm table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>id#</th>
                <th>Item</th>
                <th>Value</th>
                <th>Grade</th>
                <th>Action</th>
              </tr>
          </thead>

        </table>

      </div>
    </div>
      </div>
    </div>

</section>


<script>
$(document).ready(function() {
  getDeductions();
});
</script>
@endSection