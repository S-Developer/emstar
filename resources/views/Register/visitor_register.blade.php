@extends('MainLayouts/main')
@section('content')
<section class="content-header">
  <div class="row">
  <div class="col-md-10">
    <h5>Visitors Database</h5>
    <span>All  Visitors</span>
  </div>
  <div class="col-md-2">
   <Button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#new_vistor">Record Visitor</Button></a>
  </div>
  </div>
</section>
<section class="content">
    <div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div id="example" class="small-6 columns"></div>
        <table id="vistorstable" class="table table-bordered table-striped">
          <thead>
              <tr class="table">
                <th>Date</th>
                <th>Fullname</th>
                <th>Phone</th>
                <th>Vehicle Reg</th>
                <th>Time In</th>
                <th>Time Out</th>
              </tr>
          </thead>

        </table>
      </div>
    </div>
      </div>
    </div>

    <div id="new_vistor" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Record Visitor</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="record" method="post" action="/new_qualifications">
            <div class="row">
            <div class="form-group col-md-6" >
                <p for="inputEmail3">Fullname</p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Fullname" required>
               </div>
               <div class="form-group col-md-6">
                <p for="inputEmail3">ID Number</p>
                  <input type="text" class="form-control" id="national_id" name="national_id" placeholder="ID Number" required>
               </div>
</div>
             <div class = "row">
             <div class="form-group col-md-6">
                <p for="inputEmail3">Phone Number</p>
                  <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone" required>
               </div>
               <div class="form-group col-md-6">
                <p for="inputEmail3">Vehicle Reg</p>
                  <input type="text" class="form-control" id="vehicle_registration" name="vehicle_registration" placeholder="Number Plate" required>
               </div>
             </div>
             <div class="row">
             <div class="form-group col-md-6">
                <p for="inputEmail3">Time In</p>
                  <input type="time" class="form-control" id="time_in" name="time_in" placeholder="Time In" required>
               </div>
               <div class="form-group col-md-6">
                <p for="inputEmail3">Time Out</p>
                  <input type="time" class="form-control" id="time_out" name="time_out" placeholder="Time Out">
               </div>
</div>
<div class="row">
               <div class="form-group  col-md-12">
                <p for="inputEmail3">Date (today's default)</p>
                  <input type="date" class="form-control" id="date" name="date" required>
               </div>
</div>

               <div class="row">
               <div class="form-group col-md-6">
                <p for="inputEmail3">Office</p>
                  <input type="text" class="form-control" id="office" name="office" placeholder="office" required>
               </div>
               <div class="form-group col-md-6">
                <p for="inputEmail3">Purpose</p>
                <input type="text" class="form-control" id="purpose" name="purpose" placeholder="purpose" required>
                <input type="hidden" class="form-control" id="tmp" name="tmp" value="34.5" required>
               </div>
               </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="save_visitor_record" class="btn btn-sm btn-primary">Save</button>
          </div>
        </div>
      </form>

      </div>
    </div>

  <div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="my-modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Record Vistor Time Out</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="record_time_out">
 
 <div class="row">
                <div class="form-group  col-md-12">
                 <p for="inputEmail3">Time Out</p>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" class="form-control" id="row_id" name="row_id">
                   <input type="time" class="form-control" id="time_out" name="time_out" placeholder="Time Out" required>
                </div>
 </div>
 </form>
           </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="save_visitor_time_out" class="btn-sm btn btn-primary">Save Time Out</button>
      </div>
    </div>
  </div>
</div>








    <script>
    $(document).ready(function() {
        getVisitors();
    });
  
  </script>

</section>
@endSection