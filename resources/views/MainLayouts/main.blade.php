<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emstar_hr</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/fullcalendar/main.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../../dist/css/emstar.css">
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fas fa-user"></i>&nbsp;&nbsp;{{ Auth::user()->name }}
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
  </div>
</div>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">EmStar</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item has-treeview">
                <a href="{{'/dashboard'}}" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Dashboard
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
              </li>
          <li class="nav-item">
            <a href="{{'/employees'}}" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
              <p>
                Employees
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-piggy-bank"></i>
              <p>
                PayRoll
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../../index.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Salaries</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/Earnings'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Earnings</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/Deductions'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Deductions</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/tax'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tax Rates</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Work Leave
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{'/apply_leave'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Leave Application </p>
                </a>
              </li>

                <li class="nav-item">
                  <a href="{{'/employees_leave_applications'}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Applications </p>
                  </a>
                </li>
              <li class="nav-item">
                <a href="{{'/employees_leave'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Work Leaves </p>
                </a>
              </li>
           

            </ul>
          </li>







          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Register
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{'/all_qualification'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Today's </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/all_qualification'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employees </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/visitors'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Vistors</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
               Calendar
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="{{'/all_appointments'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Appointments</p>
                </a>
              </li>



            <li class="nav-item">
                <a href="{{'/public_holidays'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Public Holidays</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/company_events'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Our Events </p>
                </a>
              </li>


            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Mailbox
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../mailbox/mailbox.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inbox</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../mailbox/compose.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Compose</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../mailbox/read-mail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Read</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-info"></i>
              <p>
               Company
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{'/company_profile'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Company Profile </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{'/company_branding'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Company Branding </p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
               Campany DMS
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{'/new_dms'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add New</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/dms'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Documents</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
               Announcements
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{'/notices'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Notices </p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                System Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{'/all_qualification'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Qualifications </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/employee_position'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Positions </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/leave_types'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Work Leave Types </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/grades'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee Grades</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{'/dms_categories'}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dms Categories </p>
                </a>
              </li>
              <!-- #TODO configure google calendar intergration -->
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Google Meets </p>
                </a>
              </li>
               <!-- #TODO configure Teams intergration -->
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Microsoft Teams</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
                <a href="{{'/logout'}}" class="nav-link">
                  <i class="nav-icon fas fa-tree"></i>
                  <p>Signout</p>
                </a>
              </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <style>
    .content-wrapper{
      background-color:#dadada;
    }
  </style>
 <div class="content-wrapper">
   <div class="container">
   <div class="row">
                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-warning">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                </div>
            </div>
  </div>
   @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://adminlte.io">EmStar</a>.</strong>
    All rights reserved.

  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script src="../../plugins/jquery/jquery.min.js"></script>

<script src="https://unpkg.com/@fullcalendar/interaction@4.4.0/main.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/Empstar/empstar.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="../../plugins/fullcalendar/main.js"></script>
<script src="../../plugins/fullcalendar/main.min.js"></script>

<!-- Toastr -->
<script src="../../plugins/toastr/toastr.min.js"></script>
<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>

</body>
</html>
