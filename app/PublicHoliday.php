<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicHoliday extends Model
{
    //
    protected $fillable = [
        'holiday_date', 'localName', 'countryCode'
    ];
}
