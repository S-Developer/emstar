<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalInfo extends Model
{
    protected $fillable = [
        'emp_id','bloodType', 'chronic','chronicConditions',
    ];
    //
}
