<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'cmp_id','emp_id', 'title', 'fullname','phonenumber','email','end_date','start_date','url','bln_active','virtual_platform_id','bln_is_accepted','bln_is_virtual'
    ];
}