<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDms extends Model
{
    protected $fillable = [
        'cmp_id', 'title', 'description','search_key','user_id','category_id','url'
    ];
}
