<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryEarnings extends Model
{
    protected $fillable = [
        'value','item','status','grade_id'
     ];
    //
}
