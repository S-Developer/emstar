<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Qualifications extends Model
{
    protected $fillable = [
        'emp_id', 'qualification_type','description','document_id','year_attained','institution',
    ];
    //
}
