<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyEvent extends Model
{
    protected $fillable = [
        'cmp_id', 'name', 'description','date','start_time','end_time'
    ];
    //
}
