<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DmsCategory extends Model
{
    protected $fillable = [
        'cmp_id', 'name'
    ];

}
