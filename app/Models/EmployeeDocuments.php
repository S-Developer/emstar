<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDocuments extends Model
{
    protected $fillable = [
        'emp_id', 'title', 'description','document_path','added_by'
    ];
    //
}
