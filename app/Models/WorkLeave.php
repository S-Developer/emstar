<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkLeave extends Model
{

    protected $fillable = [
        'emp_id', 'leave_type', 'description','start_date','end_date','status','supporting_document_url'
    ];
    //
}
