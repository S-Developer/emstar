<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeAttendance extends Model
{
    protected $fillable = [
        'emp_id', 'date', 'time_in','time_out','comment','tmp'
    ];
}
