<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    protected $fillable = [
        'cmp_id', 'name', 'address','reg_number','date_registered','bank_acc_number','branch_code','country','logo_url','web_address','contact_email','contact_phone','bank'
    ];
    //
}
