<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryGrades extends Model
{
    protected $fillable = [
        'value','description'
     ];
    //
}
