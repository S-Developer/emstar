<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QualificationsTypes extends Model
{
    protected $fillable = [
        'qualification_type','description',
    ];
    //
}
