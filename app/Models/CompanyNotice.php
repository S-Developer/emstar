<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyNotice extends Model
{
    protected $fillable = [
        'cmp_id', 'notice','posted_by','bln_active'
    ];
    //
}
