<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    protected $fillable = [
        'emp_id', 'BankName', 'BranchCode','BranchName','AccountNumber','AccountName'
    ];
}
