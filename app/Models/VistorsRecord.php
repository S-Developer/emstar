<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VistorsRecord extends Model
{
    protected $fillable = [
        'national_id', 'full_name','phone_number','vehicle_registration','time_in','time_out','date','office','purpose','tmp'
    ];
    //
}
