<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemDomains extends Model
{
    protected $fillable = [
        'parameter', 'value','added_by',
    ];
    //
}
