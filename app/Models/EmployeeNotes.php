<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeNotes extends Model
{
    protected $fillable = [
        'emp_id','notes','Added_by'
    ];
    //
}
