<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = [
        'emp_id','company', 'position', 'description','from_datetime','to_datetime'
    ];
    //
}
