<?php

namespace App\Http\Controllers;

use App\Models\SalaryEarnings;
use App\Models\SalaryGrades;
use Illuminate\Http\Request;

class SalaryEarningsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = SalaryGrades::all();
        return view('Payroll/new_earnings', compact('grades'));

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salaryEarnings = new SalaryEarnings();
        $salaryEarnings->value=request('currency_value');
        $salaryEarnings->grade_id=request('grade_id');
        $salaryEarnings->item=request('item');
        $salaryEarnings->status='active';
        $salaryEarnings->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalaryEarnings  $salaryEarnings
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryEarnings $salaryEarnings)
    {
        $earnings = SalaryEarnings::all();
        return $earnings;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalaryEarnings  $salaryEarnings
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryEarnings $salaryEarnings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalaryEarnings  $salaryEarnings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryEarnings $salaryEarnings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalaryEarnings  $salaryEarnings
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryEarnings $salaryEarnings)
    {
        //
    }
}
