<?php

namespace App\Http\Controllers;

use App\BankDetails;
use Illuminate\Http\Request;

class BankDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function show(BankDetails $bankDetails)
    {
        $emp_id = request('emp_id');
        $banksInfo =  BankDetails::where('emp_id', $emp_id)->first();
        return view('/EmployeeCrud/bank_details',compact('banksInfo'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(BankDetails $bankDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $uniqid = request('emp_id');
        $employee = BankDetails::where('emp_id', '=', $uniqid)->first();
        $employee->update($request->all());
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankDetails $bankDetails)
    {
        //
    }
}
