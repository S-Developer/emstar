<?php

namespace App\Http\Controllers;

use App\PublicHoliday;
use Illuminate\Http\Request;

/**
 * @OA\Get(
 *     path="/projects",
 *     @OA\Response(response="200", description="Display all holidays.")
 * )
 */

class PublicHolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://date.nager.at/api/v3/PublicHolidays/2022/ZW',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $json = json_decode($response,true);

        
       
        curl_close($curl);
        foreach ($json as $itm){
            $publicHoliday = new PublicHoliday();
            $publicHoliday->holiday_date=$itm['date'];
            $publicHoliday->localName=$itm['localName'];
            $publicHoliday->countryCode=$itm['countryCode'];
            $publicHoliday->save();

        }
        echo "sucess";
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PublicHoliday  $publicHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(PublicHoliday $publicHoliday)
    {
        $publicHoliday = PublicHoliday::all();
        return $publicHoliday;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PublicHoliday  $publicHoliday
     * @return \Illuminate\Http\Response
     */
    public function edit(PublicHoliday $publicHoliday)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PublicHoliday  $publicHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PublicHoliday $publicHoliday)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PublicHoliday  $publicHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(PublicHoliday $publicHoliday)
    {
        //
    }
}
