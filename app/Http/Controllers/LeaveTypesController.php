<?php

namespace App\Http\Controllers;

use App\Models\LeaveTypes;
use Illuminate\Http\Request;

class LeaveTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $leaveTypes = new LeaveTypes();
        $leaveTypes->type=request('type');
        $leaveTypes->save();
        $leaveTypes = LeaveTypes::all();
        return view('SystemSettings/work_leave_types', compact('leaveTypes'));
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveTypes $leaveTypes)
    {
        $leaveTypes = LeaveTypes::all();
        return view('SystemSettings/work_leave_types', compact('leaveTypes'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveTypes $leaveTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveTypes $leaveTypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveTypes $leaveTypes)
    {
        //
    }
}
