<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Requests\AppointmentRequest;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id = Auth::id();
        $appointment = Appointment::where('cmp_id', '=', $id) ->get();
        return $appointment;
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentRequest $request)
    { 
        $empName = request("empName");
        $empSurname = request("empSurname");
        $title = request("title");
        $fullname = request("fullname");
        $time_range_id = request("time_range_id");

        $date = request('date');
        $time = request('time');
        $time2 = strtotime($time);
        $endTime = date("H:i", strtotime('+30 minutes', $time2));

        $s = $date . $time;
        $es = $date .$endTime;

        $start_datetime = strtotime($s);
        $end_datetime = strtotime($es);
        $start_datetime = date('Y-m-d H:i:s', $start_datetime);
        $end_datetime = date('Y-m-d H:i:s', $end_datetime);



        $title = $title ."(".$empName .$empSurname ." with ".$fullname .")";
        $id = Auth::id();
        $appointment = new  Appointment();
        $appointment->cmp_id = $id;
        $appointment->emp_id = request('emp_id');
        $appointment->title = $title;
        $appointment->fullname = request('fullname');
        $appointment->phonenumber = request('phonenumber');
        $appointment->email = request('email');
        $appointment->start_date = $start_datetime;
        $appointment->end_date = $end_datetime;
        $appointment->url = request('url');


        // TODO configure google calendar
        // url to the documentation
        //https://github.com/spatie/laravel-google-calendar

        if($appointment->save()){
            return redirect()->back()->with('success', 'Appointment saved.');

          }
          else{
            return redirect()->back()->with('error', 'An error occuied.');
          }

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
