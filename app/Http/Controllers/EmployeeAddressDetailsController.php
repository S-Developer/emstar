<?php

namespace App\Http\Controllers;

use App\EmployeeAddressDetails;
use Illuminate\Http\Request;

class EmployeeAddressDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeAddressDetails  $employeeAddressDetails
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) 
    {
        $emp_id = request('emp_id');
        $address =  EmployeeAddressDetails::where('emp_id',$emp_id)->first();
         return view('/EmployeeCrud/address_details',compact('address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeAddressDetails  $employeeAddressDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAddressDetails $employeeAddressDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeAddressDetails  $employeeAddressDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $uniqid = request('emp_id');
        $employeeAddressDetails = EmployeeAddressDetails::where('emp_id', '=', $uniqid)->first();
        $employeeAddressDetails->update($request->all());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeAddressDetails  $employeeAddressDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAddressDetails $employeeAddressDetails)
    {
        //
    }
}
