<?php

namespace App\Http\Controllers;

use App\Models\CompanyNotice;
use Illuminate\Http\Request;

class CompanyNoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CompanyNotice::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyNotice  $companyNotice
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyNotice $companyNotice)
    {
        $notices = CompanyNotice::all();
        return $notices;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyNotice  $companyNotice
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyNotice $companyNotice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyNotice  $companyNotice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyNotice $companyNotice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyNotice  $companyNotice
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyNotice $companyNotice)
    {
        //
    }
}
