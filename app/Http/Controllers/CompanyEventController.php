<?php

namespace App\Http\Controllers;

use App\Models\CompanyEvent;
use Illuminate\Http\Request;

class CompanyEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CompanyEvent::create($request->all());

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyEvent  $companyEvent
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyEvent $companyEvent)
    {
        $events = CompanyEvent::all();
        return $events;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyEvent  $companyEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyEvent $companyEvent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyEvent  $companyEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyEvent $companyEvent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyEvent  $companyEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyEvent $companyEvent)
    {
        //
    }
}
