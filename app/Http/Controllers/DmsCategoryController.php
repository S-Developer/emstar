<?php

namespace App\Http\Controllers;

use App\Models\DmsCategory;
use App\Http\Requests\DmsCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class DmsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // TODO 
        laradump()->dump("test"); 
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(DmsCategory $request)
    {
        
        $dmsCate = new DmsCategory();
        $dmsCate->cmp_id= Auth::id();
        $dmsCate->name=request('name');
        $dmsCate->save();

        if($dmsCate->id){
            return redirect()->back()->with('success', 'New Category was successfully Added.');

        }
        else{
            return redirect()->back()->with('error', 'Error Occuired.');
        }
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DmsCategory  $dmsCategory
     * @return \Illuminate\Http\Response
     */
    public function show(DmsCategory $dmsCategory)
    {
        $id = Auth::id();
        $dmsCategory = DmsCategory::where('cmp_id', '=', $id)
        ->get();
      
        return view('SystemSettings/dms_category', compact('dmsCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DmsCategory  $dmsCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(DmsCategory $dmsCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DmsCategory  $dmsCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DmsCategory $dmsCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DmsCategory  $dmsCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dmsCategory = DmsCategory::where('id', '=', $id)->first();

        if($dmsCategory->delete()){
                return redirect()->back()->with('success', ' Category was successfully deleted.');
                }
        else{
            return redirect()->back()->with('error', 'Error Occuired.');
          }
        //
    }
}
