<?php

namespace App\Http\Controllers;
// namespace HolidayAPI;
// use HolidayAPI;

use App\Models\WorkLeave;
use App\Models\LeaveTypes;
use App\Employee;
use DB;
use Illuminate\Http\Request;

class WorkLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function applyLeave(WorkLeave $workLeave){

        $leaveTypes =  LeaveTypes::all();
        $employee =  Employee::all();
        return view('EmployeeLeave/apply_leave', compact('leaveTypes','employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workLeave = new WorkLeave();
        $workLeave->supporting_document_url="";
        if ($files = $request->file('image')) {
            $fileName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('Documents'), $fileName);
            $work_leave->supporting_document_url =$fileName;
        }

        $workLeave->emp_id=request('emp_id');
        $workLeave->leave_type=request('leave_type');
        $workLeave->description=request('description');
        $workLeave->start_date=request('start_date');
        $workLeave->end_date=request('end_date');
        $workLeave->status="Pending";

        $workLeave->save();

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkLeave  $workLeave
     * @return \Illuminate\Http\Response
     */
    public function show(WorkLeave $workLeave)
    {
        $status = "Approved";
        $workLeave = WorkLeave::select('*')->where([['status', '=', $status]])->get();
        return $workLeave;
    }

    public function showCalendar(WorkLeave $work_leave){
        $status = "Approved";
        $workLeave = DB::table('employees')
        ->select('employees.FirstName')
        ->join('work_leaves','work_leaves.emp_id','=','employees.emp_id')->selectRaw('FirstName as title, start_date as start, end_date as end ,supporting_document_url as url')
        ->where('work_leaves.status', '=', $status)
        ->get();
        return $workLeave;

    }

     public function pendingApproval(WorkLeave $workLeave){
        $status = "Pending";
       $workLeave = WorkLeave::select('*')->where([['status', '=', $status]])->get();

       return $workLeave;
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkLeave  $workLeave
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkLeave $workLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkLeave  $workLeave
     * @return \Illuminate\Http\Response
     */
    public function approve($id){

        $leave_application = WorkLeave::where('id', '=', $id)->first();
        $leave_application->update(['status' => "Approved"]);
        return $leave_application;
    }

    public function reject($id){

        $leave_application = WorkLeave::where('id', '=', $id)->first();
        $leave_application->update(['status' => "Rejected"]);
        return $leave_application;
    }

    public function update(String $request)
    {
        request()->validate([
            'image' => 'required|mimes:jpeg,png,jpg,zip,pdf|max:2048',
        ]);

        if ($files = $request->file('image')) {

            $fileName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('Documents'), $fileName);


            $id = request('application_id');
            $work_leave = WorkLeave::where('id', '=', $id)->first();
            $work_leave->supporting_document_url =$fileName;
            $work_leave->save();

        }

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkLeave  $workLeave
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkLeave $workLeave)
    {
        //
    }

   
}

