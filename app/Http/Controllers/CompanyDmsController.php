<?php

namespace App\Http\Controllers;

use App\Models\CompanyDms;
use App\Models\DmsCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class CompanyDmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function add_new(){
        $id = Auth::id();
        $dmsCategory = DmsCategory::where('cmp_id', '=', $id) ->get();

        return view("Dms/new_dms",compact('dmsCategory'));

    }
    public function index()
    {
        $id = Auth::id();
        $dmsCategory =  DB::table('company_dms')
        ->join(
            'dms_categories',
            'dms_categories.id','=','company_dms.category_id',
        )
        ->where('company_dms.cmp_id',$id)
        ->get();
        
        return $dmsCategory;
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId =  Auth::id();
        if($file = $request->file('url')){
              $image_name = md5(rand(1000,10000));
              $ext = strtolower($file->getClientOriginalExtension());
              $image_full_name = $image_name.'.'.$ext;
              $uploade_path = 'uploads/images/'.$userId;
              $image_url = $uploade_path.$image_full_name;
              $file->move($uploade_path,$image_full_name);

              $dms = new CompanyDms();
              $dms->title =request('name');
              $dms->description = request('description');
              $dms->search_key = request('search_key');
              $dms->user_id = $userId;
              $dms ->cmp_id = $userId;
              $dms ->category_id = request('category_id');
              $dms ->url = $image_url;
              
              if($dms->save()){
                return redirect()->back()->with('success', 'Document saved.');

              }
              else{
                return redirect()->back()->with('error', 'An error occuied.');
              }


               
          }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CompanyDms  $companyDms
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyDms $companyDms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CompanyDms  $companyDms
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyDms $companyDms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CompanyDms  $companyDms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyDms $companyDms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CompanyDms  $companyDms
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyDms $companyDms)
    {
        //
    }
}
