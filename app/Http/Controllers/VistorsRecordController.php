<?php

namespace App\Http\Controllers;

use App\Models\VistorsRecord;
use Illuminate\Http\Request;

class VistorsRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        VistorsRecord::create($request->all());
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function timeOut(Request $request)
    {
            $row_id = request('row_id');
            $record = VistorsRecord::where('id', '=',$row_id)->first();
            $record->update($request->all());
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VistorsRecord  $vistorsRecord
     * @return \Illuminate\Http\Response
     */
    public function show(VistorsRecord $vistorsRecord)
    {
        $vistorsRecord = VistorsRecord::all();
        return $vistorsRecord;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VistorsRecord  $vistorsRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(VistorsRecord $vistorsRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VistorsRecord  $vistorsRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VistorsRecord $vistorsRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VistorsRecord  $vistorsRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(VistorsRecord $vistorsRecord)
    {
        //
    }
}
