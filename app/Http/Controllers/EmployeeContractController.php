<?php

namespace App\Http\Controllers;

use App\EmployeeContract;
use App\Models\BankDetails;
use App\EmployeeAddressDetails;
use App\Models\SalaryGrades;
use App\Models\CompanyProfile;
use App\Models\SalaryEarnings;
use App\Models\SalaryDeduction;
use App\TaxRate;
use App\Employee;
use Illuminate\Http\Request;

class EmployeeContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp_id = request('emp_id');
        $employee =  Employee::where('emp_id', $emp_id)->first();
        $salaryGrades = SalaryGrades::all();

        $salaryEarnings = SalaryEarnings::all();

        $salaryDeduction = SalaryDeduction::all();

        $taxRate = TaxRate::all();

        return view('/Salaries/new_contracts',compact('salaryDeduction','salaryGrades','salaryEarnings','taxRate','employee'));

        //
    }

    public function payslip()
    {
        $emp_id = request('emp_id');
        $employee =  Employee::where('emp_id', $emp_id)->first();
        $address = EmployeeAddressDetails ::where('emp_id', $emp_id)->first();
        $banksInfo =  BankDetails::where('emp_id', $emp_id)->first();
        $salaryGrades = SalaryGrades::all();

        $salaryEarnings = SalaryEarnings::all();

        $totalearnings =  SalaryEarnings::sum('value');
        $totaldeductions =  SalaryDeduction::sum('value');
        $net = $totalearnings - $totaldeductions;

        $salaryDeduction = SalaryDeduction::all();
        $company_profile = CompanyProfile::take(1)->first();
        $taxRate = TaxRate::all();
        return view('/Salaries/employee_payslip',compact('salaryDeduction','salaryGrades','salaryEarnings','taxRate','employee','address','company_profile','banksInfo','totalearnings','totaldeductions','net'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeContract  $employeeContract
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeContract $employeeContract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeContract  $employeeContract
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeContract $employeeContract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeContract  $employeeContract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeContract $employeeContract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeContract  $employeeContract
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeContract $employeeContract)
    {
        //
    }
}
