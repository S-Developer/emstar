<?php

namespace App\Http\Controllers;

use App\Models\EmployeeNotes;
use Illuminate\Http\Request;

class EmployeeNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employeeNotes = new EmployeeNotes();
        $employeeNotes->emp_id=request('emp_id');
        $employeeNotes->notes=request('notes');
        $employeeNotes->Added_by ="1";
        $employeeNotes->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeNotes  $employeeNotes
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeNotes $employeeNotes)
    {
        $emp_id = request('emp_id');
        $data = array(
            'emp_id'=>$emp_id,
            );
        $employeeNotes = EmployeeNotes::where('emp_id', $emp_id)->get();

        return view('EmployeeCrud/notes_details', compact('employeeNotes','data'));

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeNotes  $employeeNotes
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeNotes $employeeNotes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeNotes  $employeeNotes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeNotes $employeeNotes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeNotes  $employeeNotes
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeNotes $employeeNotes)
    {
        //
    }
}
