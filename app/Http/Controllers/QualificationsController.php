<?php

namespace App\Http\Controllers;

use App\Models\Qualifications;
use App\Models\QualificationsTypes;
use App\Models\Experience;
use Illuminate\Http\Request;

class QualificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $qualifications = new Qualifications();
        $qualifications->emp_id=request('emp_id');
        $qualifications->qualification_type=request('qualification_type');
        $qualifications->description=request('description');
        $qualifications->year_attained=request('year_attained');
        $qualifications->institution=request('institution');
        $qualifications->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Qualifications  $qualifications
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $emp_id = request('emp_id');
        $data = array(
            'emp_id'=>$emp_id,
            );
        $qualificationsTypes = QualificationsTypes::all();
        $my_qualifications = Qualifications::where('emp_id', $emp_id)->get();
        $my_experience = Experience::where('emp_id', $emp_id)->get();

        return view('EmployeeCrud/qualifications_details', compact('my_qualifications','qualificationsTypes','my_experience','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Qualifications  $qualifications
     * @return \Illuminate\Http\Response
     */
    public function edit(Qualifications $qualifications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Qualifications  $qualifications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Qualifications $qualifications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Qualifications  $qualifications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Qualifications $qualifications)
    {
        //
    }
}
