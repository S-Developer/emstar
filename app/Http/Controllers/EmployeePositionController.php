<?php

namespace App\Http\Controllers;

use App\Models\EmployeePosition;
use DB;
use App\Models\Positions;
use App\Employee;
use Illuminate\Http\Request;

class EmployeePositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $employeePosition = new EmployeePosition();
        $employeePosition->emp_id=request('emp_id');
        $employeePosition->position=request('position');
        $employeePosition->save();

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeePosition  $employeePosition
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeePosition $employeePosition)
    {

        $employeePosition = DB::table('employees')
        ->select('employees.FirstName','employees.LastName','employees.emp_id','employees.created_at','employees.updated_at','positions.position')
        ->join('employee_positions','employee_positions.emp_id','=','employees.emp_id')
        ->join('positions','positions.id','=','employee_positions.position')
        ->get();


        // ->where(['something' => 'something', 'otherThing' => 'otherThing'])


        // $employeePosition = EmployeePosition::all();
        $positions = Positions::all();
        $employee = Employee::all();
        return view('SystemSettings/positions', compact('employeePosition','positions','employee'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeePosition  $employeePosition
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeePosition $employeePosition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeePosition  $employeePosition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeePosition $employeePosition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeePosition  $employeePosition
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeePosition $employeePosition)
    {
        //
    }
}
