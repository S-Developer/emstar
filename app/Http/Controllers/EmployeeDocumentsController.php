<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDocuments;
use Illuminate\Http\Request;

class EmployeeDocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        request()->validate([
            'image' => 'required|mimes:jpeg,png,jpg,zip,pdf|max:2048',
        ]);

        if ($files = $request->file('image')) {

            $fileName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('Documents'), $fileName);

            $emp_documents = new EmployeeDocuments();
            $emp_documents->emp_id=request('emp_id');
            $emp_documents->title=request('title');
            $emp_documents->description=request('description');
            $emp_documents->document_path=$fileName;
            $emp_documents->save();
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeDocuments  $employeeDocuments
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeDocuments $employeeDocuments)
    {
        $emp_id = request('emp_id');
        $data = array(
            'emp_id'=>$emp_id,
            );
        $employeeDocuments = EmployeeDocuments::where('emp_id', $emp_id)->get();

        return view('EmployeeCrud/documents_details', compact('employeeDocuments','data'));

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeDocuments  $employeeDocuments
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeDocuments $employeeDocuments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeDocuments  $employeeDocuments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeDocuments $employeeDocuments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeDocuments  $employeeDocuments
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeDocuments $employeeDocuments)
    {
        //
    }
}
