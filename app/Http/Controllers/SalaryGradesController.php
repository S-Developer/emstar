<?php

namespace App\Http\Controllers;

use App\Models\SalaryGrades;
use Illuminate\Http\Request;

class SalaryGradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = SalaryGrades::all();
        return view('Payroll/new_deductions', compact('grades'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salaryGrades = new SalaryGrades();
        $salaryGrades->value=request('grade');
        $salaryGrades->description=request('description');
        $salaryGrades->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalaryGrades  $salaryGrades
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryGrades $salaryGrades)
    {
        $grades = SalaryGrades::all();
        return $grades;

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalaryGrades  $salaryGrades
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryGrades $salaryGrades)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalaryGrades  $salaryGrades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryGrades $salaryGrades)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalaryGrades  $salaryGrades
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryGrades $salaryGrades)
    {
        //
    }
}
