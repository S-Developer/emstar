<?php

namespace App\Http\Controllers;

use App\Models\QualificationsTypes;
use Illuminate\Http\Request;

class QualificationsTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $qualificationsTypes = new QualificationsTypes();
        $qualificationsTypes->qualification_type=request('qualification');
        $qualificationsTypes->description=request('description');
        $qualificationsTypes->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QualificationsTypes  $qualificationsTypes
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $qualifications = QualificationsTypes::all();
        return view('SystemSettings/qualifications', compact('qualifications'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QualificationsTypes  $qualificationsTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(QualificationsTypes $qualificationsTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QualificationsTypes  $qualificationsTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QualificationsTypes $qualificationsTypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QualificationsTypes  $qualificationsTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualificationsTypes $qualificationsTypes)
    {
        //
    }
}
