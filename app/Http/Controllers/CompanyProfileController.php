<?php

namespace App\Http\Controllers;

use App\Models\CompanyProfile;
use Illuminate\Http\Request;
use Carbon\Carbon;
class CompanyProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (CompanyProfile::exists()) {
            $cmp_id = request('cmp_id');
            $com_details = CompanyProfile::where('cmp_id', '=',$cmp_id)->first();
            $com_details->update($request->all());
        } else {
            $now = Carbon::now();
            $unique_code = $now->format('su');
            $profile = new CompanyProfile();
            $profile->cmp_id =$unique_code;
            $profile->name=request('name');
            $profile->address=request('address');
            $profile->reg_number=request('reg_number');
            $profile->bank_acc_number=request('bank_acc_number');
            $profile->date_registered=request('date_registered');
            $profile->branch_code=request('branch_code');
            $profile->logo_url= request('logo_url');
            $profile->web_address=request('web_address');
            $profile->contact_email=request('contact_email');
            $profile->contact_phone=request('contact_phone');
            $profile->country=request('country');
            $profile->bank=request('bank');
            $profile->save();
       
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyProfile  $companyProfile
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyProfile $companyProfile)
    {
        //
        $company_profile = CompanyProfile::take(1)->first();
        return view('/CompanyInfo/company_profile', compact('company_profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyProfile  $companyProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyProfile $companyProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyProfile  $companyProfile
     * @return \Illuminate\Http\Response
     */
    public function companyBranding(CompanyProfile $companyProfile)
    {
        //
        $company_profile = CompanyProfile::take(1)->first();
        return view('/CompanyInfo/branding_logo', compact('company_profile'));
    }

    public function updateLogo(Request $request, CompanyProfile $companyProfile)
    {
        if ($files = $request->file('image')) {

            request()->validate([
                'image' => 'required|mimes:jpeg,png,jpg,zip,pdf|max:2048',
            ]);

            $fileName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('Logo'), $fileName);

            $id = request('cmp_id');
            $companyProfile = CompanyProfile::where('cmp_id', '=', $id)->first();
            $companyProfile->logo_url ='Logo/'.$fileName;
            $companyProfile->save();

            $company_profile = CompanyProfile::take(1)->first();
            return view('/CompanyInfo/branding_logo', compact('company_profile'));

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyProfile  $companyProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyProfile $companyProfile)
    {
        //
    }
}
