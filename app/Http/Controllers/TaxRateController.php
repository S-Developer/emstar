<?php

namespace App\Http\Controllers;

use App\TaxRate;
use Illuminate\Http\Request;

class TaxRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $taxRate = new TaxRate();
        $taxRate->item=request('item'); 
        $taxRate->value=request('currency_value');
        $taxRate->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaxRate  $taxRate
     * @return \Illuminate\Http\Response
     */
    public function show(TaxRate $taxRate)
    {
        $taxRate = TaxRate::all();
        return $taxRate;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaxRate  $taxRate
     * @return \Illuminate\Http\Response
     */
    public function edit(TaxRate $taxRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaxRate  $taxRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaxRate $taxRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaxRate  $taxRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaxRate $taxRate)
    {
        //
    }
}
