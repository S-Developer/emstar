<?php

namespace App\Http\Controllers;

use App\Employee;
use App\BankDetails;
use App\Models\Experience;
use App\EmployeeAddressDetails;
use App\Models\EmployeeDocuments;
use App\Models\Qualifications;
use App\NextOfKin;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::all();
        return $employee;

    }

    public function emp_profile($emp_id,$firstname,$lastname){
        $emp_id = request('emp_id');
        $employees =  Employee::where('emp_id', $emp_id)->first();
        $nextOfKin =  NextOfKin::where('emp_id', $emp_id)->first();
        $bank =  BankDetails::where('emp_id', $emp_id)->first();
        $address =  EmployeeAddressDetails::where('emp_id', $emp_id)->first();
        $employeeDocuments = EmployeeDocuments::where('emp_id', $emp_id)->get();
        $my_qualifications = Qualifications::where('emp_id', $emp_id)->get();
        $my_experience = Experience::where('emp_id', $emp_id)->get();
        return view('/EmployeeCrud/employee_profile',compact('employees','nextOfKin','bank','address','employeeDocuments','my_experience','my_qualifications'));
    }

    public function emp_details($emp_id,$firstname,$lastname){

        $data = array(
            'emp_id'=>$emp_id,
            'firstname'=>$firstname,
            'lastname'=>$lastname
            );

        return view('EmployeeCrud/employee_details')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $now = Carbon::now();
        $unique_code = $now->format('su');
        $employee = new Employee();
        $employee->emp_id =$unique_code;
        $employee->title=request('title');
        $employee->FirstName=request('firstname');
        $employee->MiddleName=request('middlename');
        $employee->LastName=request('lastname');
        $employee->PhoneNumber=request('phone');
        $employee->IdNumber=request('idnumber');
        $employee->Email=request('email');
        $employee->DateOfBirth=request('dob');
        $employee->Status=request('status');
        $employee->Gender=request('gender');
        $employee->save();

        $address = new EmployeeAddressDetails();
        $address->emp_id=$unique_code;
        $address->save();

        $bank = new BankDetails();
        $bank ->emp_id =$unique_code;
        $bank->save();

        $next_of_kin = new NextOfKin();
        $next_of_kin ->emp_id =$unique_code;
        $next_of_kin->save();

        // $employee_documents = new EmployeeDocuments();
        // $employee_documents ->emp_id =$unique_code;
        // $employee_documents->save();

        $employee_Qualifications = new Qualifications();
        $employee_Qualifications ->emp_id =$unique_code;
        $employee_Qualifications->save();


        $data = array(
            'emp_id'=>$unique_code,
            'firstname'=>$FirstName=request('firstname'),
            'lastname'=>$LastName=request('lastname'),
            );

        return view('EmployeeCrud/employee_details')->with($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee5
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $emp_id = request('emp_id');
        $employee =  Employee::where('emp_id', $emp_id)->first();
        $nextOfKin =  NextOfKin::where('emp_id', $emp_id)->first();
        return view('/EmployeeCrud/personal_details',compact('employee','nextOfKin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $uniqid = request('emp_id');
        $employee_details = Employee::where('emp_id', '=', $uniqid)->first();
        $employee_details->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
