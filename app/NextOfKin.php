<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NextOfKin extends Model
{

    protected $fillable = [
        'emp_id', 'N_title','First_Name','Last_Name','Phone_Number','Id_Number','N_Email','Relationship'
    ];
    //
}
