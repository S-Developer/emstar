<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    protected $fillable = [
        'Name', 'NumberOfEmployes', 'Country','Address','LogoUrl',
    ];

}
