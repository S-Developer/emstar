<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAddressDetails extends Model
{
    protected $fillable = [
        'emp_id','town','city', 'address','province','county'
    ];
    //
}
